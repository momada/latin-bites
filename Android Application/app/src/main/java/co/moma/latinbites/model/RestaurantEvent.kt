package co.moma.latinbites.model

import java.io.Serializable
import java.util.*

class RestaurantEvent : Serializable{
    var documentId: String = ""
    var eventId: Int = 0
    var restaurantCode: Int = 0
    var franchiseId: Int = 0
    var eventName: String = ""
    var eventDescription: String = ""
    var image: String = ""
    var startTime = Date(0)
    var endTime = Date(0)

    constructor() {}

    constructor(documentId: String, eventId: Int, restaurantCode: Int, franchiseId: Int, eventName: String, eventDescription: String, image: String, startTime: Date, endTime: Date) {
        this.documentId = documentId
        this.eventId = eventId
        this.restaurantCode = restaurantCode
        this.franchiseId = franchiseId
        this.eventName = eventName
        this.eventDescription = eventDescription
        this.image = image
        this.startTime = startTime
        this.endTime = endTime
    }
}