package co.moma.latinbites.adapters

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import co.moma.latinbites.R
import co.moma.latinbites.model.*
import java.util.*

/**
 * Created by mfigueroa on 4/9/2018.
 */
class HorizontalRV(private var alAny: ArrayList<*>) : RecyclerView.Adapter<HorizontalRV.ViewHolder>(), View.OnClickListener {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.horizontal_rv, parent, false)
        return ViewHolder(v)
    }

    @Suppress("UNCHECKED_CAST")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val context = holder.itemView.context

        holder.v?.layoutManager = LinearLayoutManager(context, LinearLayout.HORIZONTAL, false)

        when(alAny[0]){
            is RestaurantEvent ->{
     //           holder.v?.adapter = RestaurantEventRVAdapter(alAny as ArrayList<RestaurantEvent>, context, true, Calendar.getInstance())
            }
            is PaymentMethod -> {
                holder.v?.adapter = PaymentMethodAdapter(alAny as ArrayList<PaymentMethod>)
            }
            is Promotion -> {
                holder.v?.adapter = PromotionsRV(alAny as ArrayList<Promotion>, context)
            }
        }
    }

    override fun onClick(v: View){
        when(v.id){
        }
    }

    override fun getItemCount(): Int {
        return 1
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val v = itemView.findViewById<RecyclerView>(R.id.rv_pm_hori)
    }
}
