package co.moma.latinbites.model

class Comment{
    var userName: String? = null
    var userComment: String? = null
    var userRating: Float? = null

    constructor() {}

    constructor(userName: String, userComment: String, userRating: Float){
        this.userName = userName
        this.userComment = userComment
        this.userRating = userRating
    }
}