package co.moma.latinbites.model

import java.io.Serializable

class PaymentMethod : Serializable {
    var cardBrand: String? = null
    var entryMethod: String? = null

    constructor() {}

    constructor(cardBrand: String?, entryMethod: String?) {
        this.cardBrand = cardBrand
        this.entryMethod = entryMethod
    }
}