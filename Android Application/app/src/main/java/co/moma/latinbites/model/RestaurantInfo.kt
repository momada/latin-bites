package co.moma.latinbites.model

import java.io.Serializable
import java.util.ArrayList

class RestaurantInfo : Serializable {
    var infoCode: Int = 0
    var location: String = ""
    var country: String = ""
    var address: String = ""
    var zipCode: Int = 0
    var phoneNumber: Int = 0
    var openingTime: String = ""
    var closingTime: String = ""
    var avrRating: Float = 0.0f
    var rating: ArrayList<RestaurantRating> = ArrayList()
    var foodTypes : String = ""
    var delivery: Boolean  = false

    constructor()

    constructor(infoCode: Int, location: String, country: String, address: String, zipCode: Int,
                phoneNumber: Int, openingTime: String, closingTime: String, avrRating: Float,
                rating: ArrayList<RestaurantRating>, foodTypes: String, delivery: Boolean) {
        this.infoCode = infoCode
        this.location = location
        this.country = country
        this.address = address
        this.zipCode = zipCode
        this.phoneNumber = phoneNumber
        this.openingTime = openingTime
        this.closingTime = closingTime
        this.avrRating = avrRating
        this.rating = rating
        this.foodTypes = foodTypes
        this.delivery = delivery
    }
}