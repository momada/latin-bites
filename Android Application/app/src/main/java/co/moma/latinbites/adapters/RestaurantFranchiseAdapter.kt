package co.moma.latinbites.adapters

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import co.moma.latinbites.R
import co.moma.latinbites.model.Franchise
import co.moma.latinbites.MapsActivity
import co.moma.latinbites.utils.Utility
import com.uber.sdk.android.rides.RideParameters
import com.uber.sdk.android.rides.RideRequestButton
import com.uber.sdk.android.rides.RideRequestDeeplink
import com.uber.sdk.core.auth.Scope
import com.uber.sdk.core.client.SessionConfiguration
import java.util.*


class RestaurantFranchiseAdapter(private var alFrRestaurant: ArrayList<Franchise>, val context: Context, val viewGroup: ViewGroup) : RecyclerView.Adapter<RestaurantFranchiseAdapter.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        if(alFrRestaurant[position].location.isNotEmpty()) {
            holder.tvFranchiseLocation.text = alFrRestaurant[position].location
        }
        if(alFrRestaurant[position].address.isNotEmpty()) {
            holder.tvFranchiseAddress.text = alFrRestaurant[position].address
        }
        if(alFrRestaurant[position].rating.toString().isNotEmpty()) {
            holder.tvFranchiseRating.text = alFrRestaurant[position].rating.toString()
        }
        if(alFrRestaurant[position].number.isNotEmpty()) {
            holder.tvFranchiseNumber.text = String.format("%s %s", holder.itemView.context.getString(R.string.tv_franchise_number), alFrRestaurant[position].number)
        }
        if(alFrRestaurant[position].schedule.size > 0) {
            holder.tvSchedule.text = holder.itemView.resources.getString(R.string.tv_franchise_schedule)
            alFrRestaurant[position].schedule.forEach{
                holder.tvSchedule.append(String.format("%s: %s to %s", it.openDays, it.openingTime.toString(), it.closingTime.toString()))
            }
        }
        holder.itemView.setOnClickListener{
            if(alFrRestaurant[position].longitude.toDouble() != 0.0 && alFrRestaurant[position].latitude.toDouble() != 0.0){
                val intent = Intent(context, MapsActivity::class.java)
                intent.putExtra("Franchise", alFrRestaurant[position])
                context.startActivity(intent)
            }
            else{
                Utility.showSnackbar(holder.itemView.context.getString(R.string.tv_no_franchise_coordinates), holder.itemView.rootView)
            }
        }

        holder.btTransit.setOnClickListener {

            if(alFrRestaurant[position].longitude.toFloat() != 0.0f && alFrRestaurant[position].latitude.toFloat() != 0.0f) {
                val intent = Intent(Intent.ACTION_VIEW)
                intent.data = Uri.parse(String.format("transit://directions?to=%s,%s", alFrRestaurant[position].latitude, alFrRestaurant[position].longitude))
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                context.startActivity(intent)
            }
            else {
                Utility.showSnackbar(holder.itemView.context.getString(R.string.tv_no_franchise_coordinates), holder.itemView.rootView)
            }
        }

        holder.btUber.setOnClickListener{

            val config: SessionConfiguration = SessionConfiguration.Builder()
                    .setClientId("YOUR_CLIENT_ID") //This is necessary
                    .setRedirectUri("YOUR_REDIRECT_URI") //This is necessary if you'll be using implicit grant
                    .setEnvironment(SessionConfiguration.Environment.SANDBOX) //Useful for testing your app in the sandbox environment
                    .setScopes(Arrays.asList(Scope.PROFILE, Scope.RIDE_WIDGETS)) //Your scopes for authentication here
                    .build();

            val rideParams = RideParameters.Builder()
                    .setProductId("a1111c8c-c720-46c3-8534-2fcdd730040d")
                    .setPickupToMyLocation()
                    .setDropoffLocation(alFrRestaurant[position].latitude.toDouble(), alFrRestaurant[position].latitude.toDouble(), null, alFrRestaurant[position].address)
                    .build();
            holder.btUber.setRideParameters(rideParams)

            val deepLink = RideRequestDeeplink.Builder(holder.itemView.context)
                    .setSessionConfiguration(config)
                    .setRideParameters(rideParams)
                    .build()
            deepLink.execute()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context).inflate(R.layout.model_restaurant_franchise, parent, false)
        return ViewHolder(layoutInflater, context)
    }

    override fun getItemCount(): Int {
        return alFrRestaurant.size
    }

    class ViewHolder(i_ItemView: View, val context: Context): RecyclerView.ViewHolder(i_ItemView){
        val tvFranchiseLocation = itemView.findViewById<TextView>(R.id.tv_franchise_location)
        val tvFranchiseAddress = itemView.findViewById<TextView>(R.id.tv_franchise_address)
        val tvFranchiseRating = itemView.findViewById<TextView>(R.id.tv_franchise_rating)
        val tvFranchiseNumber = itemView.findViewById<TextView>(R.id.tv_franchise_number)
        val tvPaymentMethods = itemView.findViewById<TextView>(R.id.tv_pm)
        val tvSchedule = itemView.findViewById<TextView>(R.id.tv_schedule)

        //Buttons
        val btTransit = itemView.findViewById<Button>(R.id.bt_transit)
        val btUber = itemView.findViewById<RideRequestButton>(R.id.bt_uber)
    }

}