package co.moma.latinbites.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import co.moma.latinbites.R
import co.moma.latinbites.adapters.HorizontalRV
import co.moma.latinbites.adapters.RestaurantFranchiseAdapter
import co.moma.latinbites.model.Restaurant
import com.bumptech.glide.Glide
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.activity_restaurant_detail.*
import kotlinx.android.synthetic.main.restaurant_detail.view.*
import android.widget.RatingBar
import co.moma.latinbites.activities.RestaurantDetailActivity
import kotlinx.android.synthetic.main.restaurant_detail.*


/**
 * A fragment representing a single Restaurant detail screen.
 * This fragment is either contained in a [RestaurantListActivity]
 * in two-pane mode (on tablets) or a [RestaurantDetailActivity]
 * on handsets.
 */
class RestaurantDetailFragment : Fragment() {

    /**
     * The dummy content this fragment is presenting.
     */
    private var oRestaurant: Restaurant = Restaurant()
    private var TAG = "RestaurantDetailFragment"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            if (it.containsKey(ARG_RESTAURANT)) {
                // Load the dummy content specified by the fragment
                // arguments. In a real-world scenario, use a Loader
                // to load content from a content provider.
                oRestaurant = it.getSerializable(ARG_RESTAURANT) as Restaurant
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.restaurant_detail, container, false)

        // Show the dummy content as text in a TextView.
        oRestaurant.let {

            activity?.toolbar_layout?.title = oRestaurant.name
            if(oRestaurant.description.isNotEmpty()){
                rootView.tv_details.text = oRestaurant.description
            }
            if(oRestaurant.averageRating > 0) {
                rootView.tv_avg_rating.text = oRestaurant.averageRating.toString()
            }
            if(oRestaurant.website.isNotEmpty()){
                rootView.tv_website.text = oRestaurant.website
            }
            if(oRestaurant.foodType.size > 0){
                rootView.tv_food_types.text = activity!!.applicationContext.resources.getString(R.string.tv_food_types)
                oRestaurant.foodType.forEach {
                    rootView.tv_food_types.append(it.food + "\n")
                }
            }
            if(oRestaurant.country!!.isNotEmpty()) {

                rootView.tv_country.text = String.format("%s %s", activity!!.applicationContext.resources.getString(R.string.tv_country), oRestaurant.country)
            }
            if(oRestaurant.foodType.size > 0) {
                rootView.rv_payment.layoutManager = LinearLayoutManager(activity!!.applicationContext, LinearLayout.VERTICAL, false)
                rootView.rv_payment.adapter = HorizontalRV(oRestaurant.payment)
            }
            val sName = String.format("Restaurants/%s.jpg", oRestaurant.name.toLowerCase().replace(" ",""))
            FirebaseStorage.getInstance().reference.child(sName).downloadUrl.addOnSuccessListener { it ->
                Glide.with(activity!!.iv_restaurant_backdrop.context).load(it)
                        .into(activity!!.iv_restaurant_backdrop)
            }.addOnFailureListener{
                Log.e(TAG, String.format("Source image not found: %s", it.message))
            }

            rootView.ratingBar.rating = oRestaurant.averageRating

            rootView.rl_rating.setOnClickListener{
                val formerActivity = activity as RestaurantDetailActivity
                formerActivity.CreatePopUp()
            }

            //Set user rating?

            rootView.ratingBar.onRatingBarChangeListener = RatingBar.OnRatingBarChangeListener { ratingBar, v, b ->
                Log.d(TAG, "Rating changed $v")
            }

            rootView.rv_restaurant_franchises.layoutManager = LinearLayoutManager(activity!!.applicationContext, LinearLayout.VERTICAL, false)
            rootView.rv_restaurant_franchises.adapter = RestaurantFranchiseAdapter(oRestaurant.franchises, activity!!.applicationContext, rootView.rv_restaurant_franchises)
        }

        return rootView
    }

    fun updateRating(newRating: Float){
        userRatingBar.rating = newRating
        //TODO("Update firebase information")
    }

    companion object {
        /**
         * The fragment argument representing the item ID that this fragment
         * represents.
         */
        const val ARG_RESTAURANT = "item_id"
        const val ARG_FAVORITES = "item_favorites"
    }
}
