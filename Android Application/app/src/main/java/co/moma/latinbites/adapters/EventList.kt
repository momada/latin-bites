package co.moma.latinbites.adapters

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import co.moma.latinbites.EventDetails
import co.moma.latinbites.R
import co.moma.latinbites.model.RestaurantEvent
import kotlin.collections.ArrayList

class EventList(private var alRestaurantEvents: ArrayList<RestaurantEvent>) : RecyclerView.Adapter<EventList.ViewHolder>() {

    private var TAG = "EventList"

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvEventName.text = alRestaurantEvents[position].eventName
        holder.tvEventPromotionalText.text = alRestaurantEvents[position].eventDescription

        holder.itemView.setOnClickListener{
            try{

                val intent = Intent(holder.itemView.context, EventDetails::class.java)
                intent.putExtra("RestaurantEvent", alRestaurantEvents[position])
                holder.itemView.context.startActivity(intent)

            }
            catch(ex:Exception){

                Log.d(TAG, ex.message.toString())

            }
        }
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.events_list, parent, false))
    }

    override fun getItemCount(): Int {
        return alRestaurantEvents.size
    }

    class ViewHolder(i_ItemView: View): RecyclerView.ViewHolder(i_ItemView){
        val tvEventName = i_ItemView.findViewById<TextView>(R.id.tv_event_name)
        val tvEventPromotionalText = i_ItemView.findViewById<TextView>(R.id.tv_event_text)
    }

}