package co.moma.latinbites.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import co.moma.latinbites.R
import co.moma.latinbites.model.RestaurantEvent

/**
 * A fragment representing a single RestaurantEvent detail screen.
 * This fragment is either contained in a [EventListActivity]
 * in two-pane mode (on tablets) or a [EventDetailActivity]
 * on handsets.
 */
class RestaurantEventDetailFragment : Fragment() {

    /**
     * The dummy content this fragment is presenting.
     */
    private var oRestaurantEvent: RestaurantEvent = RestaurantEvent()
    private var TAG = "RestaurantEventDetailFragment"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            if (it.containsKey(ARG_RESTAURANT_EVENT)) {
                // Load the dummy content specified by the fragment
                // arguments. In a real-world scenario, use a Loader
                // to load content from a content provider
                oRestaurantEvent = it.getSerializable(ARG_RESTAURANT_EVENT) as RestaurantEvent
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.event_detail, container, false)

        // Show the dummy content as text in a TextView.
        oRestaurantEvent.let {

            //activity?.toolbar_layout?.title
        }

        return rootView
    }

    companion object {
        /**
         * The fragment argument representing the item ID that this fragment
         * represents.
         */
        const val ARG_RESTAURANT_EVENT = "item_id"
    }
}
