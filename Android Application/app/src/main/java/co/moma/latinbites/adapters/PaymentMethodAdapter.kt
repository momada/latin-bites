package co.moma.latinbites.adapters

import android.support.design.widget.Snackbar
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import co.moma.latinbites.R
import co.moma.latinbites.model.PaymentMethod

class PaymentMethodAdapter(val cardList: ArrayList<PaymentMethod>) : RecyclerView.Adapter<PaymentMethodAdapter.ViewHolder>(), View.OnClickListener {

    override fun onBindViewHolder(holder: PaymentMethodAdapter.ViewHolder, position: Int) {
//        holder?.ivCard?.setImageResource(cardList[position].productImage)
        holder.tvCard?.text = cardList[position].cardBrand
    }

    override fun onClick(v: View){
        when(v.id){
          R.id.idCardName ->{
            Snackbar.make(v, "We accept this card!", Snackbar.LENGTH_LONG).show()
        }
        }
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.model_restaurant_pm, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return cardList.size
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val ivCard = itemView.findViewById<ImageView>(R.id.idCardImage)
        val tvCard = itemView.findViewById<TextView>(R.id.idCardName)
    }
}
