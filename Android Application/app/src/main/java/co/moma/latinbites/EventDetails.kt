package co.moma.latinbites

import android.os.Bundle
import android.util.Log
import co.moma.latinbites.model.RestaurantEvent

class EventDetails : BaseActivity() {

    private var TAG = "EventDetails"
    private var oEvent = RestaurantEvent()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event_details)
        setCheckedItem(2)

        if (intent.hasExtra("RestaurantEvent")){
            Log.i(TAG, "Got event")

            oEvent = intent.getSerializableExtra("RestaurantEvent") as RestaurantEvent

        }
    }
}
