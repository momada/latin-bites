package co.moma.latinbites

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.AdapterView
import co.moma.latinbites.model.RestaurantFilter
import co.moma.latinbites.utils.LocaleManager
import kotlinx.android.synthetic.main.activity_filter.*


class RestaurantFilterActivity : AppCompatActivity() {

    var rf : RestaurantFilter = RestaurantFilter(0,0,0)

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(LocaleManager.onAttach(newBase))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_filter)

        setSpinnerPositioning()

        btn_near_me.setOnClickListener { it ->
            val returnIntent = Intent()
            returnIntent.putExtra(RestaurantActivity.ARG_NEAR_ME, true)
            setResult(RESULT_OK, returnIntent)
            finish()
        }

        fab.setOnClickListener { view ->
            val returnIntent = Intent()
            returnIntent.putExtra("filter", rf)
            setResult(RESULT_OK, returnIntent)
            finish()
        }
        sp_country.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                rf.country = position
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }
        }

        sp_food.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                rf.food = position
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }
        }
    }

    fun setSpinnerPositioning() {

        if(intent != null) {
            rf = intent.getSerializableExtra("filter") as RestaurantFilter
        }
        sp_country.setSelection(rf.country)
        sp_food.setSelection(rf.food)
    }
}
