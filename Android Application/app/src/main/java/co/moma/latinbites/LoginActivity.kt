package co.moma.latinbites

import android.Manifest.permission.READ_CONTACTS
import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.annotation.TargetApi
import android.app.LoaderManager.LoaderCallbacks
import android.content.CursorLoader
import android.content.Intent
import android.content.Loader
import android.content.pm.PackageManager
import android.database.Cursor
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.provider.ContactsContract
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import com.facebook.*
import com.facebook.appevents.AppEventsLogger
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.*
import kotlinx.android.synthetic.main.activity_login.*
import java.util.*

/**
 * A login screen that offers login via email/password.
 */
class LoginActivity : AppCompatActivity(), LoaderCallbacks<Cursor> {
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private var mAuthTask: UserLoginTask? = null
    lateinit var mAuth:FirebaseAuth
    private var callbackManager: CallbackManager? = null
    private var mGoogleSingInClient: GoogleSignInClient? = null
    private var RC_SIGN_IN = 100
    private var task: Task<GoogleSignInAccount>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        setSupportActionBar(tb_settings)
        tb_settings.title = title

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        // Set up the login form.
        populateAutoComplete()
        mAuth = FirebaseAuth.getInstance();
        password.setOnEditorActionListener(TextView.OnEditorActionListener { _, id, _ ->
            if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                attemptLogin()
                return@OnEditorActionListener true
            }
            false
        })


        FacebookSdk.sdkInitialize(applicationContext)
        AppEventsLogger.activateApp(applicationContext)

        try {
            val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestIdToken(getString(R.string.default_web_client_id))
                    .requestEmail()
                    .build()
            mGoogleSingInClient = GoogleSignIn.getClient(applicationContext, gso)

            val account = GoogleSignIn.getLastSignedInAccount(applicationContext)
            btn_google.setOnClickListener { signIn() }

            mAuth = FirebaseAuth.getInstance()
            callbackManager = CallbackManager.Factory.create()

            btn_facebook.setReadPermissions("email", "public_profile")

            btn_facebook.registerCallback(callbackManager, object :
                    FacebookCallback<LoginResult> {
                override fun onSuccess(result: LoginResult?) {
                    Toast.makeText(applicationContext, R.string.login_successful, Toast.LENGTH_LONG).show()
                    if (!handleFacebookAccessToken(result!!.accessToken)) {
                        LoginManager.getInstance().logOut()
                    }

                }

                override fun onCancel() {
                    Toast.makeText(applicationContext, R.string.login_cancel, Toast.LENGTH_LONG).show()
                }

                override fun onError(error: FacebookException?) {
                    Toast.makeText(applicationContext, R.string.login_error, Toast.LENGTH_LONG).show()
                }
            })

        } catch (ex: Exception) {
            Log.d("UserCreationOptions", ex.message.toString())
        }

        email_sign_in_button.setOnClickListener { attemptLogin() }
    }

    override fun onOptionsItemSelected(item: MenuItem) =
            when (item.itemId) {
                android.R.id.home -> {
                    // This ID represents the Home or Up button. In the case of this
                    // activity, the Up button is shown. For
                    // more details, see the Navigation pattern on Android Design:
                    //
                    // http://developer.android.com/design/patterns/navigation.html#up-vs-back

                    navigateUpTo(Intent(this, Main::class.java))
                    true
                }
                else -> super.onOptionsItemSelected(item)
            }

    fun updateUI() {
        val intent = Intent(this, Main::class.java)
        startActivity(intent)
    }


    fun handleFacebookAccessToken(token : AccessToken)  : Boolean {
        Log.d("home", "handleFacebookAccessToken:" + token);
        var bSuccess = false
        val credential: AuthCredential = FacebookAuthProvider.getCredential(token.token)

        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this) { taskId ->

                    if (taskId.isSuccessful) {
                        Log.d("home", "signInWithCredential:success");
                        val user = mAuth.currentUser;
                        bSuccess = true;
                        updateUI()


                    } else {
                        Log.w("home", "signInWithCredential:failure", taskId.getException());

                        showMessage(taskId.exception!!.message.toString())

                        Toast.makeText(this, taskId.exception!!.message ,Toast.LENGTH_LONG).show();
                        taskId.exception
                    }
                }
                .addOnFailureListener(this) { exception ->
                    Log.w("home", exception.message)
                    if (exception is FirebaseAuthInvalidCredentialsException) {
                        showMessage("Invalid password")
                    } else if (exception is FirebaseAuthInvalidUserException) {

                        val errorCode = exception.errorCode

                        if (errorCode == "ERROR_USER_NOT_FOUND") {
                            showMessage("No matching account found")
                        } else if (errorCode == "ERROR_USER_DISABLED") {
                            showMessage("User account has been disabled")
                        } else {
                            showMessage(exception.message!!)
                        }
                    } else if(exception is FirebaseAuthUserCollisionException) {
                        val errorCode = exception.errorCode

                        if(errorCode == "ERROR_ACCOUNT_EXISTS_WITH_DIFFERENT_CREDENTIAL"){
                            showMessage("Account already created with another service")
                        }
                    }
                }
        return bSuccess
    }


    fun showMessage( message:String){
        Snackbar.make(findViewById(R.id.ly_login), message, Snackbar.LENGTH_INDEFINITE).setAction("Dismiss") {
            fun Onclick(v: View) {
            }
        }.show()
    }

    private fun populateAutoComplete() {
        if (!mayRequestContacts()) {
            return
        }

        loaderManager.initLoader(0, null, this)
    }

    private fun mayRequestContacts(): Boolean {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true
        }
        if (checkSelfPermission(READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            return true
        }
        if (shouldShowRequestPermissionRationale(READ_CONTACTS)) {
            Snackbar.make(email, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok
                    ) { requestPermissions(arrayOf(READ_CONTACTS), REQUEST_READ_CONTACTS) }
        } else {
            requestPermissions(arrayOf(READ_CONTACTS), REQUEST_READ_CONTACTS)
        }
        return false
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        if (requestCode == REQUEST_READ_CONTACTS) {
            if (grantResults.size == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                populateAutoComplete()
            }
        }
    }

    /**    Google Sign In */
    fun signIn() {
        showProgress(true)
        val singInIntent = mGoogleSingInClient!!.signInIntent
        startActivityForResult(singInIntent, RC_SIGN_IN)

    }


    fun handleSingInResult(completedTask : Task<GoogleSignInAccount>?){
        try {
            val account = completedTask!!.exception
        } catch ( e : ApiException){
            Log.w("UserCreationOptions", "signInResult:failed code=" + e.statusCode)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager!!.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_SIGN_IN) {
            task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSingInResult(task)

            val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)

            if (result.isSuccess)
            {

                val  account = result.signInAccount
                firebaseAuthWithGoogle(account!!)
            }
            else
            {
                Toast.makeText(applicationContext, R.string.login_error, Toast.LENGTH_LONG).show()
            }
        }
    }

    fun firebaseAuthWithGoogle(acct : GoogleSignInAccount):Boolean{
        Log.d("UserCreationOptions","firebaseAuthWithGoogle:" + acct.id)

        showProgress(false)

        var bSuccess = false;
        val credential = GoogleAuthProvider.getCredential(acct.idToken,null)
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this) { taskId ->

                    if (taskId.isSuccessful) {
                        Log.d("home", "signInWithCredential:success");
                        val user = mAuth.currentUser;
                        bSuccess = true
                        updateUI()


                    } else {
                        Log.w("home", "signInWithCredential:failure", taskId.getException());
                        Toast.makeText(this, taskId.exception!!.message ,Toast.LENGTH_LONG).show();

                    }
                }
        return bSuccess
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private fun attemptLogin() {
        if (mAuthTask != null) {
            return
        }

        // Reset errors.
        email.error = null
        password.error = null

        // Store values at the time of the login attempt.
        val emailStr = email.text.toString()
        val passwordStr = password.text.toString()

        var cancel = false
        var focusView: View? = null

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(passwordStr) && !isPasswordValid(passwordStr)) {
            password.error = getString(R.string.error_invalid_password)
            focusView = password
            cancel = true
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(emailStr)) {
            email.error = getString(R.string.error_field_required)
            focusView = email
            cancel = true
        } else if (!isEmailValid(emailStr)) {
            email.error = getString(R.string.error_invalid_email)
            focusView = email
            cancel = true
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView?.requestFocus()
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true)


            Log.d("Home","createAccount:" + emailStr)

            mAuth.createUserWithEmailAndPassword(emailStr, passwordStr)
                    .addOnCompleteListener(this) { task ->
                        showProgress(false)
                        if(task.isSuccessful){
                            val user = mAuth.currentUser
                            Toast.makeText(this, user!!.displayName, Toast.LENGTH_LONG).show()
                        } else {
                            Toast.makeText(this, task.exception!!.message.toString(), Toast.LENGTH_LONG).show()
                        }

                    }

            //mAuthTask = UserLoginTask(emailStr, passwordStr)
            //mAuthTask!!.execute(null as Void?)
        }
    }

    private fun isEmailValid(email: String): Boolean {
        //TODO: Replace this with your own logic
        return email.contains("@")
    }

    private fun isPasswordValid(password: String): Boolean {
        //TODO: Replace this with your own logic
        return password.length > 4
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private fun showProgress(show: Boolean) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            val shortAnimTime = resources.getInteger(android.R.integer.config_shortAnimTime).toLong()

            login_form.visibility = if (show) View.GONE else View.VISIBLE
            login_form.animate()
                    .setDuration(shortAnimTime)
                    .alpha((if (show) 0 else 1).toFloat())
                    .setListener(object : AnimatorListenerAdapter() {
                        override fun onAnimationEnd(animation: Animator) {
                            login_form.visibility = if (show) View.GONE else View.VISIBLE
                        }
                    })

            login_progress.visibility = if (show) View.VISIBLE else View.GONE
            login_progress.animate()
                    .setDuration(shortAnimTime)
                    .alpha((if (show) 1 else 0).toFloat())
                    .setListener(object : AnimatorListenerAdapter() {
                        override fun onAnimationEnd(animation: Animator) {
                            login_progress.visibility = if (show) View.VISIBLE else View.GONE
                        }
                    })
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            login_progress.visibility = if (show) View.VISIBLE else View.GONE
            login_form.visibility = if (show) View.GONE else View.VISIBLE
        }
    }

    override fun onCreateLoader(i: Int, bundle: Bundle?): Loader<Cursor> {
        return CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE + " = ?", arrayOf(ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE),

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC")
    }

    override fun onLoadFinished(cursorLoader: Loader<Cursor>, cursor: Cursor) {
        val emails = ArrayList<String>()
        cursor.moveToFirst()
        while (!cursor.isAfterLast) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS))
            cursor.moveToNext()
        }

        addEmailsToAutoComplete(emails)
    }

    override fun onLoaderReset(cursorLoader: Loader<Cursor>) {

    }

    private fun addEmailsToAutoComplete(emailAddressCollection: List<String>) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        val adapter = ArrayAdapter(this@LoginActivity,
                android.R.layout.simple_dropdown_item_1line, emailAddressCollection)

        email.setAdapter(adapter)
    }

    object ProfileQuery {
        val PROJECTION = arrayOf(
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY)
        val ADDRESS = 0
        val IS_PRIMARY = 1
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    inner class UserLoginTask internal constructor(private val mEmail: String, private val mPassword: String) : AsyncTask<Void, Void, Boolean>() {

        override fun doInBackground(vararg params: Void): Boolean? {
            // TODO: attempt authentication against a network service.

            try {



            } catch (e: InterruptedException) {
                return false
            }

            return DUMMY_CREDENTIALS
                    .map { it.split(":") }
                    .firstOrNull { it[0] == mEmail }
                    ?.let {
                        // Account exists, return true if the password matches.
                        it[1] == mPassword
                    }
                    ?: true
        }

        override fun onPostExecute(success: Boolean?) {
            mAuthTask = null
            showProgress(false)

            if (success!!) {
                finish()
            } else {
                password.error = getString(R.string.error_incorrect_password)
                password.requestFocus()
            }
        }

        override fun onCancelled() {
            mAuthTask = null
            showProgress(false)
        }
    }

    companion object {

        /**
         * Id to identity READ_CONTACTS permission request.
         */
        private val REQUEST_READ_CONTACTS = 0

        /**
         * A dummy authentication store containing known user names and passwords.
         * TODO: remove after connecting to a real authentication system.
         */
        private val DUMMY_CREDENTIALS = arrayOf("foo@example.com:hello", "bar@example.com:world")
    }
}
