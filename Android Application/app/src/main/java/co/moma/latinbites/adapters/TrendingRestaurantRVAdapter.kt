package co.moma.latinbites.adapters


import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Bundle
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import co.moma.latinbites.R
import co.moma.latinbites.activities.RestaurantDetailActivity
import co.moma.latinbites.fragments.RestaurantDetailFragment
import co.moma.latinbites.model.eCountry
import co.moma.latinbites.model.RestaurantFilter
import co.moma.latinbites.model.Restaurant
import com.bumptech.glide.Glide
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.model_trending_restaurant.view.*

/**
 * Created by mfigueroa on 3/18/2018.
 */
class TrendingRestaurantRVAdapter(private val parentActivity: RestaurantDetailActivity,
                                  private var userList: ArrayList<Restaurant>,
                                  private var bTrending: Boolean,
                                  private val twoPane: Boolean) :
        RecyclerView.Adapter<TrendingRestaurantRVAdapter.ViewHolder>() {

    private var m_bFiltering = false
    private var filteredRestaurants = ArrayList<Restaurant>()
    private var mFirstUse = true
    private var TAG = "RestaurantRVAdapter"
    private val onClickListener: View.OnClickListener


    init {
        onClickListener = View.OnClickListener { v ->
            val item = v.tag as Restaurant
            if (twoPane) {
                val fragment = RestaurantDetailFragment().apply {
                    arguments = Bundle().apply {
                        putString(RestaurantDetailFragment.ARG_RESTAURANT, item.documentId)
                    }
                }
                parentActivity.supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.restaurant_detail_container, fragment)
                        .commit()
            } else {
                val intent = Intent(v.context, RestaurantDetailActivity::class.java).apply {
                    putExtra(RestaurantDetailFragment.ARG_RESTAURANT, item.documentId)
                }
                v.context.startActivity(intent)
            }
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        if(m_bFiltering) {
            holder.tvRestaurantName.text = filteredRestaurants[position].name
            holder.tvRestaurantInfo.text = filteredRestaurants[position].website
            holder.tvRestaurantRating.text = filteredRestaurants[position].averageRating.toString()
        }
        else {
            holder.tvRestaurantName.text = userList[position].name
            holder.tvRestaurantInfo.text = userList[position].website
            holder.tvRestaurantRating.text = userList[position].averageRating.toString()
        }

        holder.itemView.setOnClickListener{
            try{
                val intent = Intent(holder.itemView.context, RestaurantDetailActivity::class.java)
                intent.putExtra("DocumentID", if(m_bFiltering) filteredRestaurants[position] else userList[position])
                holder.itemView.context.startActivity(intent)
            }catch(ex:Exception){
                Log.d("RestaurantRVAdapter", ex.message.toString())
            }
        }
        val sName = String.format("Restaurants/%s.jpg",
                if(m_bFiltering) filteredRestaurants[position].name.toLowerCase().replace(" ","")
                else userList[position].name.toLowerCase().replace(" ",""))
        FirebaseStorage.getInstance().reference.child(sName).downloadUrl.addOnSuccessListener {it ->

            Glide.with(holder.ivRestaurant.context).load(it)
                    .into(holder.ivRestaurant)
        }.addOnFailureListener{
            Log.e(TAG, String.format("Source image not found: %s", it.message))
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater
                .from(parent.context)
                .inflate(R.layout.model_trending_restaurant, parent, false))
    }

    override fun onViewRecycled(holder: ViewHolder) {
        super.onViewRecycled(holder)
        holder.ivRestaurant.setImageBitmap(BitmapFactory.decodeResource(holder.itemView.context.resources, R.drawable.ic_latin_vibes))
    }

    override fun getItemCount(): Int {
        when(m_bFiltering){
            true -> return filteredRestaurants.size
            false -> return userList.size
        }
    }

    private fun clearFilter() {
        filteredRestaurants.clear()
        m_bFiltering= false;
    }

    fun updateRestaurants(restaurant: ArrayList<Restaurant>) {
        DiffUtil.calculateDiff(PhotoRowDiffCallback(restaurant, userList), false).dispatchUpdatesTo(this)
        userList = restaurant
        clearFilter()
    }

    fun getCurrentRestaurantList(): ArrayList<Restaurant> {
        when(m_bFiltering){
            true -> return filteredRestaurants
            false -> return userList
        }
    }

    fun canOpenMap(): Boolean {
        var bResult = false

        var currentList = ArrayList<Restaurant>()

        if(m_bFiltering) {
            currentList = filteredRestaurants
        }
        else {
            currentList = userList
        }

        currentList.forEach { it ->
            it.franchises.forEach { it2 ->
                if(it2.longitude.toDouble() != 0.0 && it2.latitude.toDouble() != 0.0) {
                    bResult = true
                    return@forEach
                }
            }
        }

        return bResult
    }

    fun filterRestaurant(filter: RestaurantFilter) {
        m_bFiltering= true

        val sCountryFilter = eCountry.values()[filter.country].toString()

        val newRestaurants = userList.filter {
            simpleRestaurant -> simpleRestaurant.country!!.equals(sCountryFilter)
            //|| simpleRestaurant.foodType.contains(eFoodType.values()[filter.food].name)
        } as ArrayList<Restaurant>

        if(newRestaurants.size == userList.size && !mFirstUse) {
            DiffUtil.calculateDiff(PhotoRowDiffCallback(newRestaurants, filteredRestaurants), false).dispatchUpdatesTo(this)
        }
        else {
            DiffUtil.calculateDiff(PhotoRowDiffCallback(newRestaurants, userList), false).dispatchUpdatesTo(this)
            mFirstUse = false
        }
        filteredRestaurants = newRestaurants
    }

    fun filterRestaurant(query: String) {
        m_bFiltering= true

        val newRestaurants = userList.filter { simpleRestaurant ->
            simpleRestaurant.name.toLowerCase().contains(query.toLowerCase()) || simpleRestaurant.country!!.toLowerCase()!!.contains(query.toLowerCase())
            //|| simpleRestaurant.foodType.contains(query)
        } as ArrayList<Restaurant>

        if(newRestaurants.size == userList.size && !mFirstUse) {
            DiffUtil.calculateDiff(PhotoRowDiffCallback(newRestaurants, filteredRestaurants), false).dispatchUpdatesTo(this)
        }
        else {
            DiffUtil.calculateDiff(PhotoRowDiffCallback(newRestaurants, userList), false).dispatchUpdatesTo(this)
            mFirstUse = false
        }
        filteredRestaurants = newRestaurants

    }

    class PhotoRowDiffCallback(private val newRows : List<Restaurant>, private val oldRows : List<Restaurant>) : DiffUtil.Callback() {
        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val oldRow = oldRows[oldItemPosition]
            val newRow = newRows[newItemPosition]
            return oldRow == newRow
        }

        override fun getOldListSize(): Int = oldRows.size

        override fun getNewListSize(): Int = newRows.size

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val oldRow = oldRows[oldItemPosition]
            val newRow = newRows[newItemPosition]
            return oldRow == newRow
        }
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

        val tvRestaurantName: TextView = itemView.tv_trending_restaurant_name
        val tvRestaurantInfo: TextView = itemView.tv_trending_restaurant_info
        val tvRestaurantRating: TextView = itemView.tv_trending_restaurant_rating
        val ivRestaurant: ImageView = itemView.iv_trending_restaurant
    }

}