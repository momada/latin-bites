package co.moma.latinbites.model

import java.io.Serializable
import java.util.*

class Schedule : Serializable {
    var openDays: String? = null
    var openingTime: Date? = null
    var closingTime: Date? = null

    constructor() {}

    constructor(day: String?, openingTime: Date?, closingTime: Date?) {
        this.openDays = day
        this.openingTime = openingTime
        this.closingTime = closingTime
    }
}