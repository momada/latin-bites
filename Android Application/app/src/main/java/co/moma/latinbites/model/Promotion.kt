package co.moma.latinbites.model

import java.io.Serializable

class Promotion : Serializable {
    var documentID: String = ""
    var promotionID: Int = 0
    var restaurantCode: Int = 0
    var franchiseID: Int = 0
    var promotionName: String = ""
    var promotionText: String = ""
    var logo: String = ""

    constructor() {}

    constructor(documentID: String, promotionID: Int, restaurantCode: Int, franchiseID: Int, promotionName: String, promotionText: String, logo: String) {
        this.documentID = documentID
        this.promotionID = promotionID
        this.restaurantCode = restaurantCode
        this.franchiseID = franchiseID
        this.promotionName = promotionName
        this.promotionText = promotionText
        this.logo = logo
    }
}