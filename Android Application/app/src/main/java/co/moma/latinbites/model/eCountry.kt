package co.moma.latinbites.model

enum class eCountry {
    Argentina,
    Colombia,
    Mexico,
    Venezuela,
}