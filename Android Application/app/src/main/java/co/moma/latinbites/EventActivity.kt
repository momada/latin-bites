package co.moma.latinbites

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.SearchView
import co.moma.latinbites.adapters.RestaurantEventRVAdapter
import co.moma.latinbites.model.RestaurantEvent
import co.moma.latinbites.model.EventFilter
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.Query
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.event_list.*
import kotlinx.android.synthetic.main.vertical_rv.*
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class EventActivity : BaseActivity(), SearchView.OnQueryTextListener {

    private val TAG = "EventActivity"
    private var mFilter: EventFilter = EventFilter(0,0,0)
    private var alRestaurantEvents: ArrayList<RestaurantEvent> = ArrayList()
    private var hmRestaurantEvent: HashMap<Int, ArrayList<RestaurantEvent>> = HashMap()
    private var eventAdapter: RestaurantEventRVAdapter? = null
    private var twoPane: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.vertical_rv)
        setCheckedItem(2)

        if (event_detail_container != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            twoPane = true
        }

        rv_vertical.layoutManager = LinearLayoutManager(applicationContext, LinearLayout .VERTICAL, false)
        eventAdapter = RestaurantEventRVAdapter(this, hmRestaurantEvent, applicationContext, false, Calendar.getInstance(), twoPane)
        rv_vertical.adapter = eventAdapter
        fastscroll.setRecyclerView(rv_vertical)

        getEvents(rv_vertical, activity_progress)
    }

    fun getEvents(view: View, progressBar: ProgressBar ){

        showProgress(true, view, progressBar)

        val RestaurantEventQuery: Query = mDatabaseReference!!.child("RestaurantEvents")

        RestaurantEventQuery.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                dataSnapshot.children.forEach { children ->
                    val nRestaurantEvent = children.getValue(RestaurantEvent::class.java)
                    nRestaurantEvent!!.documentId = children.key!!
                    alRestaurantEvents.add(nRestaurantEvent)
                    Log.d(TAG, "Value is: " + nRestaurantEvent.eventDescription)
                }

                hmRestaurantEvent = orderEventsByDay(alRestaurantEvents)

                //Set event adapter
                rv_vertical.layoutManager = LinearLayoutManager(applicationContext, LinearLayout .VERTICAL, false)
                eventAdapter = RestaurantEventRVAdapter(this@EventActivity, hmRestaurantEvent, applicationContext, false, Calendar.getInstance(), twoPane)
                rv_vertical.adapter = eventAdapter

                //Remove waiting
                showProgress(false, view, progressBar)
            }

            override fun onCancelled(error: DatabaseError) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException())
                //Remove waiting
                showProgress(false, view, progressBar)
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.home, menu)
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager

        val mItem = menu.findItem(R.id.action_search) as MenuItem
        val searchView: SearchView = mItem.actionView as SearchView

        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        searchView.setIconifiedByDefault(true)
        searchView.setOnQueryTextListener(this)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_nearMe -> return true
            R.id.action_filter -> {
                val intent = Intent(this, RestaurantFilterActivity::class.java)
                intent.putExtra("filter", mFilter)
                startActivityForResult(intent,1)
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    fun orderEventsByDay(alRestaurantEvents: ArrayList<RestaurantEvent>) : HashMap<Int, ArrayList<RestaurantEvent>>{
        val mEvents: HashMap<Int, ArrayList<RestaurantEvent>> = HashMap()
        var alCurrentRestaurantEvent: ArrayList<RestaurantEvent> = ArrayList()
        val calendar = Calendar.getInstance()
        var dCurrent = 1
        var iCurrentKey = 0

        if(alRestaurantEvents.size == 0) return HashMap()
        
        mEvents.put(iCurrentKey, ArrayList())

        //First openDays
        calendar.time = alRestaurantEvents[0].startTime

        alRestaurantEvents.forEach{

            //First we get latest added openDays and compare it with the new one.
            val oCalendar = Calendar.getInstance()
            oCalendar.time = it.startTime


            if(oCalendar.get(Calendar.DAY_OF_MONTH) != calendar.get(Calendar.DAY_OF_MONTH)){
                iCurrentKey++
                calendar.time = it.startTime
                mEvents.put(iCurrentKey, ArrayList())
            }
            alCurrentRestaurantEvent = mEvents[iCurrentKey] as ArrayList<RestaurantEvent>
            alCurrentRestaurantEvent.add(it)
        }

        return mEvents
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        if(newText != null)
        {
            //eventAdapter!!.filterEvent(newText)
        }
        return true
    }
}
