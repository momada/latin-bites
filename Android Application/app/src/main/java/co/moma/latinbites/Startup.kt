package co.moma.latinbites

import android.Manifest
import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.os.Handler
import android.os.ResultReceiver
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import kotlinx.android.synthetic.main.activity_startup.*
import com.google.android.gms.tasks.OnFailureListener
import android.location.Geocoder
import co.moma.latinbites.model.Constants
import co.moma.latinbites.utils.FetchAddress
import co.moma.latinbites.utils.Utility
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.tasks.OnSuccessListener
class Startup : AppCompatActivity() {

    private var TAG = "Statup"
    private var lastLocation: Location? = null
    private var resultReceiver: AddressResultReceiver ? = null
    private var addressOutput: String ? = null
    private var mAddressOutput: String ? = null

    private var mFusedLocationClient: FusedLocationProviderClient ? = null

    private val REQUEST_PERMISSIONS_REQUEST_CODE = 34

    private val ADDRESS_REQUESTED_KEY = "address-request-pending"
    private val LOCATION_ADDRESS_KEY = "location-address"

    internal inner class AddressResultReceiver(handler: Handler) : ResultReceiver(handler) {

        override fun onReceiveResult(resultCode: Int, resultData: Bundle?) {
            addressOutput = resultData?.getString(Constants.RESULT_DATA_KEY)

            if(resultCode == Constants.SUCCESS_RESULT){
                et_location.setText(addressOutput)
            }
            else {
                Utility.showSnackbar("Could not get your location", rl_startup)
            }
        }
    }

    override fun onStart() {
        super.onStart()

        if (!Utility.checkPermissions(this, ACCESS_FINE_LOCATION)) {
            requestPermissions();
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_startup)

        resultReceiver = AddressResultReceiver(Handler())

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        bt_find_me.setOnClickListener { getAddress() }

        bt_ok.setOnClickListener{
            val intent = Intent(this, Main::class.java)
            intent.putExtra("location", lastLocation)
            startActivity(intent)
        }

        bt_sign_in.setOnClickListener{
            startActivity(Intent(this, LoginActivity::class.java))
        }
    }

    private fun getLocation() {
        val intent = Intent(this, FetchAddress::class.java)
        intent.putExtra(Constants.RECEIVER, resultReceiver)
        intent.putExtra(Constants.LOCATION_DATA_EXTRA, lastLocation)
        startService(intent)
    }

    private fun requestPermissions() {
        val shouldProvideRationale = ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_FINE_LOCATION)

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.")

            showSnackbar(R.string.permission_rationale, android.R.string.ok,
                    View.OnClickListener {
                        // Request permission
                        ActivityCompat.requestPermissions(this@Startup,
                                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                                REQUEST_PERMISSIONS_REQUEST_CODE)
                    })

        } else {
            Log.i(TAG, "Requesting permission")
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    REQUEST_PERMISSIONS_REQUEST_CODE)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                                            grantResults: IntArray) {
        Log.i(TAG, "onRequestPermissionResult")
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.size <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i(TAG, "User interaction was cancelled.")
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission granted.
                //getAddress()
            } else {
                // Permission denied.

                // Notify the user via a SnackBar that they have rejected a core permission for the
                // app, which makes the Activity useless. In a real app, core permissions would
                // typically be best requested during a welcome-screen flow.

                // Additionally, it is important to remember that a permission might have been
                // rejected without asking the user for permission (device policy or "Never ask
                // again" prompts). Therefore, a user interface affordance is typically implemented
                // when permissions are denied. Otherwise, your app could appear unresponsive to
                // touches or interactions which have required permissions.
                /*showSnackbar(R.string.permission_denied_explanation, R.string.settings,
                        View.OnClickListener {
                            // Build intent that displays the App settings screen.
                            val intent = Intent()
                            intent.action = ACTION_APPLICATION_DETAILS_SETTINGS
                            val uri = Uri.fromParts("package",
                                    BuildConfig.APPLICATION_ID, null)
                            intent.data = uri
                            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                            startActivity(intent)
                        })*/
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun getAddress() {
        mFusedLocationClient!!.lastLocation
                .addOnSuccessListener(this, OnSuccessListener<Location> { location ->
                    if (location == null) {
                        Log.w(TAG, "onSuccess:null")
                        return@OnSuccessListener
                    }
                    lastLocation = location

                    if(lastLocation != null){
                        getLocation()
                    }

                    // Determine whether a Geocoder is available.
                    if (!Geocoder.isPresent()) {
                        Utility.showSnackbar("No geocoder available", rl_startup)
                        return@OnSuccessListener
                    }

                    // If the user pressed the fetch address button before we had the location,
                    // this will be set to true indicating that we should kick off the intent
                    // service after fetching the location.
                    /*if (mAddressRequested) {
                        startIntentService()
                    }*/
                })
                .addOnFailureListener(this, OnFailureListener { e -> Log.w(TAG, "getLastLocation:onFailure", e) })
    }

    companion object {
        private val REQUEST_LOCATION_ACCESS = 0
    }

    private fun showSnackbar(mainTextStringId: Int, actionStringId: Int,
                                  listener: View.OnClickListener) {
        Snackbar.make(findViewById(android.R.id.content),
                getString(mainTextStringId),
                Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(actionStringId), listener).show()
    }
}
