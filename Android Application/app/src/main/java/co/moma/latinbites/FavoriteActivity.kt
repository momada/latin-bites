package co.moma.latinbites

import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.SearchView
import co.moma.latinbites.adapters.RestaurantRVAdapter
import co.moma.latinbites.model.Restaurant
import co.moma.latinbites.model.UserFavorite
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.Query
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.vertical_rv.*
import java.util.ArrayList

class FavoriteActivity : BaseActivity(), SearchView.OnQueryTextListener {

    private val TAG = "RestaurantActivity"
    private var alFavoriteRestaurant: ArrayList<Restaurant> = ArrayList()
    private var restaurantAdapter: RestaurantRVAdapter? = null
    private var twoPane: Boolean = false
    private var searchView: SearchView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.vertical_rv)
        setCheckedItem(3)

        rv_vertical.layoutManager = LinearLayoutManager(applicationContext, LinearLayout.VERTICAL, false)
        restaurantAdapter = RestaurantRVAdapter(this, alFavoriteRestaurant, mUserFavorite, false, twoPane)
        rv_vertical.adapter = restaurantAdapter
        fastscroll.setRecyclerView(rv_vertical)

        getFavoriteRestaurants(rv_vertical, activity_progress)
    }

    fun getFavoriteRestaurants(view: View, progressBar: ProgressBar) {
        showProgress(true, view, progressBar)

        val mFavoriteQuery = mDatabaseReference!!.child("UserRating").child(mUserFavorite.userName)

        mFavoriteQuery.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onDataChange(p0: DataSnapshot) {
                if(p0.value != null)  {
                    mUserFavorite = p0.getValue(UserFavorite::class.java)!!
                }
                //Now we filter restaurants
                filterRestaurants(view, progressBar)
            }
        })
    }

    fun filterRestaurants(view: View, progressBar: ProgressBar) {

        val mRestaurantQuery: Query = mDatabaseReference!!.child("Restaurants").orderByChild("name")

        mRestaurantQuery.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                val allRestaurant = ArrayList<Restaurant>()

                dataSnapshot.children.forEach { children ->
                    val nRestaurant = children.getValue(Restaurant::class.java)
                    nRestaurant!!.documentId = children.key!!
                    allRestaurant.add(nRestaurant)
                    Log.d(TAG, "Value is: " + nRestaurant.name)

                }
                mUserFavorite.favoriteRestaurants.forEach { iDocument ->
                    val nRestaurant = allRestaurant.find { it -> it.documentId == iDocument }
                    if(nRestaurant != null) {
                        alFavoriteRestaurant.add(nRestaurant)
                    }
                }

                restaurantAdapter = RestaurantRVAdapter(this@FavoriteActivity, alFavoriteRestaurant, mUserFavorite, false, twoPane)
                rv_vertical.adapter = restaurantAdapter
                fastscroll.setRecyclerView(rv_vertical)
                rv_vertical.adapter.notifyDataSetChanged()
                //Remove waiting3
                showProgress(false, view, progressBar)
            }

            override fun onCancelled(error: DatabaseError) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException())
                //Remove waiting
                showProgress(false, view, progressBar)
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.

        menuInflater.inflate(R.menu.home, menu)
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager

        val mItem = menu.findItem(R.id.action_search) as MenuItem
        searchView = mItem.actionView as SearchView

        searchView!!.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        searchView!!.setIconifiedByDefault(true)
        searchView!!.setOnQueryTextListener(this)
        searchView!!.clearFocus()
        return true
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        searchView!!.clearFocus()
        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        if(newText != null)
        {
            restaurantAdapter!!.filterRestaurant(newText)
        }
        return true
    }
}
