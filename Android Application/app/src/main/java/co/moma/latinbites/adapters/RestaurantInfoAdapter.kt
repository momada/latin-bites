package co.moma.latinbites.adapters

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RatingBar
import android.widget.TextView
import co.moma.latinbites.R
import co.moma.latinbites.model.Restaurant
import com.bumptech.glide.Glide
import com.google.firebase.storage.FirebaseStorage

/**
 * Created by mfigueroa on 4/9/2018.
 */
class RestaurantInfoAdapter(var selectedRestaurant: Restaurant) : RecyclerView.Adapter<RestaurantInfoAdapter.ViewHolder>(){
    private var TAG = "RestaurantInfoAdapter"

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvRestaurantName?.text = selectedRestaurant.name
        if(selectedRestaurant.description.isNotEmpty()){
            holder.tvDetails.text = selectedRestaurant.description
        }
        if(selectedRestaurant.averageRating > 0) {
            holder.tvAvgRating?.text = selectedRestaurant.averageRating.toString()
        }
        if(selectedRestaurant.website.isNotEmpty()){
            holder.tvWebsite.text = selectedRestaurant.website
        }
        if(selectedRestaurant.foodType.size > 0){

            holder.tvFranchiseFoodTypes.text = holder.itemView.context.resources.getString(R.string.tv_food_types)
            selectedRestaurant.foodType.forEach {
                holder.tvFranchiseFoodTypes.append(it.food + "\n")
            }

        }
        if(selectedRestaurant.country!!.isNotEmpty()) {

            holder.tvFranchiseCountry?.text = String.format("%s %s", holder.itemView.context.resources.getString(R.string.tv_country), selectedRestaurant.country)
        }
        if(selectedRestaurant.foodType.size > 0) {
            holder.rvPayment.layoutManager = LinearLayoutManager(holder.itemView.context, LinearLayout.VERTICAL, false)
            holder.rvPayment.adapter = HorizontalRV(selectedRestaurant.payment)
        }
        val sName = String.format("Restaurants/%s.jpg", selectedRestaurant.name.toLowerCase().replace(" ",""))
        FirebaseStorage.getInstance().reference.child(sName).downloadUrl.addOnSuccessListener { it ->
            Glide.with(holder.ivRestaurntLogo.context).load(it)
                    .into(holder.ivRestaurntLogo)
        }.addOnFailureListener{
            Log.e(TAG, String.format("Source image not found: %s", it.message))
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.model_restaurant_info_rv, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return 1
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        //Main card
        val ivRestaurntLogo = itemView.findViewById<ImageView>(R.id.iv_restaurant_detail)
        val tvDetails = itemView.findViewById<TextView>(R.id.tv_details)
        val tvRestaurantName = itemView.findViewById<TextView>(R.id.tv_restaurant_info_name)
        val tvWebsite = itemView.findViewById<TextView>(R.id.tv_website)
        val rbRestaurant = itemView.findViewById<RatingBar>(R.id.ratingBar)
        val tvAvgRating = itemView.findViewById<TextView>(R.id.tv_avg_rating)
        val tvRestaurantPayment = itemView.findViewById<TextView>(R.id.tv_payment)
        val tvFranchiseFoodTypes = itemView.findViewById<TextView>(R.id.tv_food_types)
        val tvFranchiseCountry = itemView.findViewById<TextView>(R.id.tv_country)
        val rvPayment = itemView.findViewById<RecyclerView>(R.id.rv_payment)
    }

}