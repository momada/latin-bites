package co.moma.latinbites.adapters

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import co.moma.latinbites.MapsActivity
import co.moma.latinbites.R
import co.moma.latinbites.model.Franchise

class FranchiseAdapter(val frRestaurant: Franchise, val context: Context) : RecyclerView.Adapter<FranchiseAdapter.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.tvLocation.text = frRestaurant.location
        holder.tvAddress.text = frRestaurant.address

        frRestaurant.schedule.forEach {
            holder.tvSchedule.append("Opened on" + it.openDays + ", from " +
            it.openingTime + " to " + it.closingTime + "\n")
        }
        holder.tvAvgRating.text = frRestaurant.rating.toString()
        holder.tvLastComment.text = frRestaurant.comment[0].userComment


        holder.btMap.setOnClickListener{ v ->
            context.startActivity(Intent(context, MapsActivity::class.java))
        }
        holder.btMore.setOnClickListener{ v ->
            TODO("Add comment view")
        }
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.model_franchise, parent, false)
        return ViewHolder(v, frRestaurant, context)
    }

    override fun getItemCount(): Int {
        return 1
    }

    class ViewHolder(itemView: View, frRestaurant: Franchise, val context: Context): RecyclerView.ViewHolder(itemView) {

        //TextViews
        val tvLocation = itemView.findViewById<TextView>(R.id.tv_f_location)
        val tvAddress = itemView.findViewById<TextView>(R.id.tv_f_address)
        val tvSchedule = itemView.findViewById<TextView>(R.id.tv_f_schedule)
        val tvAvgRating = itemView.findViewById<TextView>(R.id.tv_f_rating_avg)
        val tvLastComment = itemView.findViewById<TextView>(R.id.tv_f_comment)

        //Buttons
        val btMap = itemView.findViewById<Button>(R.id.bt_map)
        val btMore = itemView.findViewById<Button>(R.id.bt_more)
    }
}