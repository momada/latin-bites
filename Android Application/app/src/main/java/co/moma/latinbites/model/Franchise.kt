package co.moma.latinbites.model

import java.io.Serializable
import java.util.ArrayList

class Franchise: Serializable {

    var id: Int = 0
    var location: String = ""
    var address: String = ""
    var number: String = ""
    var delivery: Boolean = false
    var longitude: String = ""
    var latitude: String = ""
    var rating: Float = 0f
    var comment: ArrayList<Comment> = ArrayList()
    var schedule: ArrayList<Schedule> = ArrayList()


    constructor() {}

    constructor(id: Int, location: String, address: String, number: String, delivery: Boolean, longitude: String, latitude: String, rating: Float, comment: ArrayList<Comment>, schedule: ArrayList<Schedule>) {
        this.id = id
        this.location = location
        this.address = address
        this.number = number
        this.delivery = delivery
        this.longitude = longitude
        this.latitude = latitude
        this.rating = rating
        this.comment = comment
        this.schedule = schedule
    }
}