package co.moma.latinbites.model

import java.io.Serializable
import java.util.*

/**
 * Created by mfigueroa on 3/18/2018.
 */
class Restaurant : Serializable{
    var code: Int = 0
    var name: String = ""
    var website: String = ""
    var description: String = ""
    var enabled: Boolean = false
    var country: String? = ""
    var logo: String = ""
    var foodType: ArrayList<FoodType> = ArrayList()
    var franchises: ArrayList<Franchise> = ArrayList()
    var averageRating: Float = 0f
    var payment: ArrayList<PaymentMethod> = ArrayList()
    var documentId: String = ""

    constructor() {}

    constructor(code: Int, name: String, website: String, description: String, enabled: Boolean,
                country: String?, logo: String, foodType: ArrayList<FoodType>,
                franchises: ArrayList<Franchise>, averageRating: Float,
                payment: ArrayList<PaymentMethod>, documentId: String) {
        this.code = code
        this.name = name
        this.website = website
        this.description = description
        this.enabled = enabled
        this.country = country
        this.logo = logo
        this.foodType = foodType
        this.franchises = franchises
        this.averageRating = averageRating
        this.payment = payment
        this.documentId = documentId
    }


}