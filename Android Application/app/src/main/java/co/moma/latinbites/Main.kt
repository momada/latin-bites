package co.moma.latinbites

import android.location.Location
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.LinearSnapHelper
import android.util.Log
import android.widget.LinearLayout
import co.moma.latinbites.adapters.HorizontalRV
import co.moma.latinbites.adapters.HorizontalRestaurantRVAdapter
import co.moma.latinbites.model.RestaurantEvent
import co.moma.latinbites.model.Promotion
import co.moma.latinbites.model.Restaurant
import co.moma.latinbites.model.UserFavorite
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.Query
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_main.*

class Main : BaseActivity() {

    var lastLocation: Location? = null
    val TAG = "Main"
    private val alRestaurant = ArrayList<Restaurant>()
    private var mRestaurantAdapter: HorizontalRestaurantRVAdapter? = null
    private val alRestaurantEvents = ArrayList<RestaurantEvent>()
    private val alPromotion = ArrayList<Promotion>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        super.setContentView(R.layout.activity_main)
        setCheckedItem(0)

        if (intent != null) {
            Log.i(TAG, "location has been retrieved" + lastLocation)
            lastLocation = intent.getParcelableExtra("location")

            if (lastLocation == null) {

            }
        }
        val snapHelper = LinearSnapHelper()
        snapHelper.attachToRecyclerView(rv_trending_restaurant)
        //Set all adapters
        setAllAdapter()

        getFavorites()
    }

    fun getFavorites() {
        //We should get favorite restaurant list
        val mFavoriteQuery = mDatabaseReference!!.child("UserRating").child(mUserFavorite.userName)

        mFavoriteQuery.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                if(dataSnapshot.value != null)  {
                    mUserFavorite = dataSnapshot.getValue(UserFavorite::class.java)!!
                }

                //Get actual information and reset adapters if available
                getTrendingRestaurant()
                //getEvents()
                //getPromotions()
            }

            override fun onCancelled(error: DatabaseError) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException())
            }
        })
    }

    fun getTrendingRestaurant() {

        val mRestaurantQuery: Query = mDatabaseReference!!.child("Restaurants").orderByChild("name")

        mRestaurantQuery.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                dataSnapshot.children.forEach { children ->
                    val nRestaurant = children.getValue(Restaurant::class.java)
                    nRestaurant!!.documentId = children.key!!
                    alRestaurant.add(nRestaurant)
                    Log.d(TAG, "Value is: " + nRestaurant.name)
                }
                setRestaurantAdapter()
            }

            override fun onCancelled(error: DatabaseError) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException())
            }
        })
    }

    fun getEvents() {

        val RestaurantEventQuery: Query = mDatabaseReference!!.child("RestaurantEvents")

        RestaurantEventQuery.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                dataSnapshot.children.forEach { children ->
                    val nRestaurantEvent = children.getValue(RestaurantEvent::class.java)
                    nRestaurantEvent!!.documentId = children.key!!
                    alRestaurantEvents.add(nRestaurantEvent)
                    Log.d(TAG, "Value is: " + nRestaurantEvent.eventDescription)
                }
                //Set event adapter
                setEventsAdapter()
            }

            override fun onCancelled(error: DatabaseError) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException())
            }
        })
    }

    fun getPromotions() {

        //TBD
    }

    fun setRestaurantAdapter() {
        rv_trending_restaurant.layoutManager = LinearLayoutManager(applicationContext, LinearLayout.VERTICAL, false)
        mRestaurantAdapter = HorizontalRestaurantRVAdapter(this@Main, alRestaurant, mUserFavorite)
        rv_trending_restaurant.adapter = mRestaurantAdapter
        rv_trending_restaurant.adapter.notifyDataSetChanged()
    }

    fun setEventsAdapter() {
        rv_trending_events.layoutManager = LinearLayoutManager(applicationContext, LinearLayout.VERTICAL, false)
        val trendingAdapter = HorizontalRV(alRestaurantEvents)
        rv_trending_events.adapter = trendingAdapter
    }

    fun setPromotionsAdapter() {
        rv_promotions.layoutManager = LinearLayoutManager(applicationContext, LinearLayout.VERTICAL, false)
        val promotionAdapter = HorizontalRV(alPromotion)
        rv_promotions.adapter = promotionAdapter
    }

    fun setAllAdapter() {
        setRestaurantAdapter()
        //setEventsAdapter()
        //setPromotionsAdapter()
    }
}