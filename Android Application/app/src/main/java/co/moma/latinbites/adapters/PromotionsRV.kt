package co.moma.latinbites.adapters


import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import co.moma.latinbites.R
import co.moma.latinbites.activities.RestaurantDetailActivity
import co.moma.latinbites.model.Promotion

/**
 * Created by mfigueroa on 3/18/2018.
 */
class PromotionsRV(private var alPromotions: ArrayList<Promotion>, val context: Context) : RecyclerView.Adapter<PromotionsRV.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        //holder.ivPromotion
        holder.tvPromotionName.text = alPromotions[position].promotionName
        holder.tvPromotionText.text = alPromotions[position].promotionText
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context).inflate(R.layout.model_promotions, parent, false)
        return ViewHolder(layoutInflater, alPromotions, context)
    }

    override fun getItemCount(): Int {
        return alPromotions.size
    }

    class ViewHolder(i_ItemView: View, i_alPromotions: ArrayList<Promotion>, val i_Context: Context): RecyclerView.ViewHolder(i_ItemView), View.OnClickListener {

        val tvPromotionName = itemView.findViewById<TextView>(R.id.tv_promotion_name)
        val tvPromotionText = itemView.findViewById<TextView>(R.id.tv_promotion_text)
        val ivPromotion = itemView.findViewById<ImageView>(R.id.iv_promotion)
        val view = itemView.setOnClickListener(this)
        val lPromotion = i_alPromotions

        override fun onClick(v: View?) {
            val position = adapterPosition

            try{
                val intent = Intent(i_Context, RestaurantDetailActivity::class.java)
                intent.putExtra("code", lPromotion[position].restaurantCode)
                intent.putExtra("infoCode", lPromotion[position].franchiseID)
                intent.putExtra("iEventCode", lPromotion[position].promotionID)
                i_Context.startActivity(intent)
            }catch(ex:Exception){
                Log.d("RestaurantRVAdapter", ex.message.toString())
            }
        }
    }

}