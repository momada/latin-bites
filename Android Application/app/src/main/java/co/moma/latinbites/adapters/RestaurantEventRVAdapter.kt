package co.moma.latinbites.adapters


import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import co.moma.latinbites.R
import co.moma.latinbites.activities.RestaurantEventDetailActivity
import co.moma.latinbites.fragments.RestaurantDetailFragment
import co.moma.latinbites.fragments.RestaurantEventDetailFragment
import co.moma.latinbites.model.RestaurantEvent
import java.text.SimpleDateFormat
import java.util.Calendar
import kotlin.collections.ArrayList

/**
 * Created by mfigueroa on 3/18/2018.
 */
class RestaurantEventRVAdapter(private val parentActivity: AppCompatActivity,
                               private var hmEvents: HashMap<Int, ArrayList<RestaurantEvent>>,
                               val context: Context,
                               var bTrending: Boolean,
                               var calendar: Calendar,
                               private val twoPane: Boolean) :
        RecyclerView.Adapter<RestaurantEventRVAdapter.ViewHolder>() {

    private var TAG = "RestaurantEventRVAdapter"
    private var alFilteredRestaurantEvents: ArrayList<RestaurantEvent> = ArrayList()
    private val onClickListener: View.OnClickListener

    init {
        onClickListener = View.OnClickListener { v ->
            val item = v.tag as RestaurantEvent
            if (twoPane) {
                val fragment = RestaurantEventDetailFragment().apply {
                    arguments = Bundle().apply {
                        putSerializable(RestaurantDetailFragment.ARG_RESTAURANT, item)
                    }
                }
                parentActivity.supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.restaurant_detail_container, fragment)
                        .commit()
            } else {
                val intent = Intent(v.context, RestaurantEventDetailActivity::class.java).apply {
                    putExtra(RestaurantDetailFragment.ARG_RESTAURANT, item)
                }
                v.context.startActivity(intent)
            }
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val event = hmEvents[position] as ArrayList<RestaurantEvent>

        val currentDate = event[0].startTime

        val format = SimpleDateFormat("EEEE - dd, MMMM")

        holder.tvDay.text = format.format(currentDate)
        holder.rvList.layoutManager = LinearLayoutManager(holder.itemView.context, LinearLayout .VERTICAL, false)
        holder.rvList.adapter = EventList(event)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater
                .from(parent.context)
                .inflate(R.layout.activity_events, parent, false))
    }

    override fun getItemCount(): Int {
            return hmEvents.size
        }

    inner class ViewHolder(i_ItemView: View): RecyclerView.ViewHolder(i_ItemView) {
        val tvDay = itemView.findViewById<TextView>(R.id.tv_current_day)
        val rvList = itemView.findViewById<RecyclerView>(R.id.rv_events_list)
    }
}