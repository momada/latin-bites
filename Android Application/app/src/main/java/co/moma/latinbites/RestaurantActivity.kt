package co.moma.latinbites

import android.annotation.SuppressLint
import android.app.Activity
import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.SearchView
import co.moma.latinbites.adapters.RestaurantRVAdapter
import co.moma.latinbites.model.RestaurantFilter
import co.moma.latinbites.model.Restaurant
import co.moma.latinbites.utils.Utility
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.restaurant_list.*
import kotlinx.android.synthetic.main.vertical_rv.*
import java.util.ArrayList


class RestaurantActivity : BaseActivity(), SearchView.OnQueryTextListener {

    private val TAG = "RestaurantActivity"
    private var mFilter: RestaurantFilter = RestaurantFilter(0,0,0)
    private var mbCanOpenMap = false
    private var alRestaurant: ArrayList<Restaurant> = ArrayList()
    private var restaurantAdapter: RestaurantRVAdapter? = null
    private var twoPane: Boolean = false
    private var searchView: SearchView ? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.vertical_rv)
        setCheckedItem(1)

        if (restaurant_detail_container != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            twoPane = true
        }

        rv_vertical.layoutManager = LinearLayoutManager(applicationContext, LinearLayout.VERTICAL, false)
        restaurantAdapter = RestaurantRVAdapter(this, alRestaurant, mUserFavorite, false, twoPane)
        rv_vertical.adapter = restaurantAdapter
        fastscroll.setRecyclerView(rv_vertical)

        getSimpleRestaurants(rv_vertical, activity_progress)
    }

    fun getSimpleRestaurants(view: View, progressBar: ProgressBar) {

        showProgress(true, view, progressBar)

        val mRestaurantQuery: Query = mDatabaseReference!!.child("Restaurants").orderByChild("name")

        mRestaurantQuery.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                dataSnapshot.children.forEach { children ->
                    val nRestaurant = children.getValue(Restaurant::class.java)
                    nRestaurant!!.documentId = children.key!!
                    alRestaurant.add(nRestaurant)
                    Log.d(TAG, "Value is: " + nRestaurant.name)
                }

                restaurantAdapter = RestaurantRVAdapter(this@RestaurantActivity, alRestaurant, mUserFavorite, false, twoPane)
                rv_vertical.adapter = restaurantAdapter
                fastscroll.setRecyclerView(rv_vertical)
                rv_vertical.adapter.notifyDataSetChanged()
                //Remove waiting
                showProgress(false, view, progressBar)
            }

            override fun onCancelled(error: DatabaseError) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException())
                //Remove waiting
                showProgress(false, view, progressBar)
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.

        menuInflater.inflate(R.menu.home, menu)
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager

        val mItem = menu.findItem(R.id.action_search) as MenuItem
        searchView = mItem.actionView as SearchView

        searchView!!.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        searchView!!.setIconifiedByDefault(true)
        searchView!!.setOnQueryTextListener(this)
        searchView!!.clearFocus()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_nearMe -> {
                if(restaurantAdapter!!.canOpenMap()){
                    val intent = Intent(this, MapsActivity::class.java)
                    intent.putExtra("alRestaurants", restaurantAdapter!!.getCurrentRestaurantList())
                    startActivity(intent)
                }
                else {
                    Utility.showSnackbar(applicationContext.getString(R.string.tv_no_franchise_coordinates), window.decorView.rootView)
                }
                return true
            }
            R.id.action_filter -> {
                val intent = Intent(this, RestaurantFilterActivity::class.java)
                intent.putExtra("filter", mFilter)
                startActivityForResult(intent,1)
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    @SuppressLint("MissingPermission")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 1){
            if(resultCode == Activity.RESULT_OK){

                if(data!!.getBooleanExtra(ARG_NEAR_ME, false)) {
                    mFilter = RestaurantFilter(0,0,0)
                    mFusedLocationClient!!.lastLocation
                            .addOnSuccessListener(this, OnSuccessListener<Location> { location ->
                                if (location == null) {
                                    Log.w(TAG, "onSuccess:null")
                                    return@OnSuccessListener
                                }
                                (rv_vertical.adapter as RestaurantRVAdapter).filterNearMe(location)
                            })
                            .addOnFailureListener(this) {
                                e -> Log.w(TAG, "getLastLocation:onFailure", e)
                            }
                }
                else {
                    mFilter = data!!.getSerializableExtra("filter") as RestaurantFilter
                    (rv_vertical.adapter as RestaurantRVAdapter).filterRestaurant(mFilter)
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        searchView!!.clearFocus()
        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        if(newText != null)
        {
            restaurantAdapter!!.filterRestaurant(newText)
        }
        return true
    }

    companion object {
        const val ARG_FILTER = "filter"
        const val ARG_NEAR_ME = "near_me"
    }
}
