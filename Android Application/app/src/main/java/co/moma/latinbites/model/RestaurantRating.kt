package co.moma.latinbites.model

import java.io.Serializable

class RestaurantRating : Serializable {

    var userName: String = ""
    var rating: Int = 0
    var comments: ArrayList<RestaurantComments> = ArrayList()

    constructor()

    constructor(userName: String, rating: Int, comments: ArrayList<RestaurantComments>) {
        this.userName = userName
        this.rating = rating
        this.comments = comments
    }
}