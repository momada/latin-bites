package co.moma.latinbites

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.widget.LinearLayout
import co.moma.latinbites.adapters.HorizontalRV
import co.moma.latinbites.model.Card
import kotlinx.android.synthetic.main.activity_franchise.*

class FranchiseActivity : BaseActivity() {

    private var sDocumentID: String = ""
    private var iFranchiseId: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_franchise)
        setCheckedItem(1)


        if(intent != null){
            sDocumentID = intent.getStringExtra("DocumentID")
            iFranchiseId = intent.getIntExtra("iFranchiseId", 0)
        }

        val cardList = ArrayList<Card>()
        cardList.add(Card(1, "VISA"))
        cardList.add(Card(2, "MasterCard"))
        cardList.add(Card(2, "AMEX"))
        cardList.add(Card(2, "Discover"))

        rv_franchise_pm.layoutManager = LinearLayoutManager(applicationContext, LinearLayout.HORIZONTAL, false)
        val adapter2 = HorizontalRV(cardList)
        rv_franchise_pm.adapter = adapter2
    }
}
