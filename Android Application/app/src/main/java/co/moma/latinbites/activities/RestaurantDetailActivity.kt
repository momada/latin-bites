package co.moma.latinbites.activities

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.transition.Slide
import android.transition.TransitionManager
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.LinearLayout
import android.widget.PopupWindow
import android.widget.RatingBar
import co.moma.latinbites.R
import co.moma.latinbites.R.id.action_favorite
import co.moma.latinbites.R.id.action_unfavorite
import co.moma.latinbites.fragments.RestaurantDetailFragment
import co.moma.latinbites.model.Restaurant
import co.moma.latinbites.model.UserFavorite
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_restaurant_detail.*

/**
 * An activity representing a single Restaurant detail screen. This
 * activity is only used on narrow width devices. On tablet-size devices,
 * item details are presented side-by-side with a list of items
 * in a [RestaurantListActivity].
 */
class RestaurantDetailActivity : AppCompatActivity() {

    var mFragment: RestaurantDetailFragment ?= null
    var mRestaurant: Restaurant = Restaurant()
    val TAG = "RestaurantDetail"
    var m_bFavoritedRestaurant = false
    var mFirebaseDatabase: FirebaseDatabase? = null
    var mDatabaseReference: DatabaseReference? = null
    var mUserFavorite: UserFavorite = UserFavorite()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_restaurant_detail)
        setSupportActionBar(detail_toolbar)

        mFirebaseDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mFirebaseDatabase!!.reference

        // Show the Up button in the action bar.
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        // savedInstanceState is non-null when there is fragment state
        // saved from previous configurations of this activity
        // (e.g. when rotating the screen from portrait to landscape).
        // In this case, the fragment will automatically be re-added
        // to its container so we don't need to manually add it.
        // For more information, see the Fragments API guide at:
        //
        // http://developer.android.com/guide/components/fragments.html
        //
        if (savedInstanceState == null) {
            // Create the detail fragment and add it to the activity
            // using a fragment transaction.
            mRestaurant = intent.getSerializableExtra(RestaurantDetailFragment.ARG_RESTAURANT) as Restaurant
            mUserFavorite = intent.getSerializableExtra(RestaurantDetailFragment.ARG_FAVORITES) as UserFavorite

            mFragment = RestaurantDetailFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(RestaurantDetailFragment.ARG_RESTAURANT,
                            mRestaurant)
                }
            }
            supportFragmentManager.beginTransaction()
                    .add(R.id.restaurant_detail_container, mFragment)
                    .commit()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_detail, menu)
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        //If the restaurant exists on the list, then we should just set it as favorited
        if(mUserFavorite.favoriteRestaurants.contains(mRestaurant.documentId)) {
            m_bFavoritedRestaurant = true
        }

        if(m_bFavoritedRestaurant) {
            menu!!.findItem(R.id.action_favorite).isVisible = false
            menu.findItem(R.id.action_unfavorite).isVisible = true
        }
        else {
            menu!!.findItem(R.id.action_favorite).isVisible = true
            menu.findItem(R.id.action_unfavorite).isVisible = false
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) =
            when (item.itemId) {
                android.R.id.home -> {
                    // This ID represents the Home or Up button. In the case of this
                    // activity, the Up button is shown. For
                    // more details, see the Navigation pattern on Android Design:
                    //
                    // http://developer.android.com/design/patterns/navigation.html#up-vs-back

                    super.onBackPressed()
                    //navigateUpTo(Intent(this, RestaurantActivity::class.java))
                    true
                }
                action_favorite -> {
                    registerFavoriteRestaurant()
                    invalidateOptionsMenu()
                    true
                }
                action_unfavorite -> {
                    unregisterFavoriteRestaurant()
                    invalidateOptionsMenu()
                    true
                }
                else -> super.onOptionsItemSelected(item)
            }


    private fun registerFavoriteRestaurant() {
        Log.d(TAG, String.format("Set favorite restaurant = %s", mRestaurant.name))

        mUserFavorite.favoriteRestaurants.add(mRestaurant.documentId)

        mDatabaseReference!!.child("UserRating").child(mUserFavorite.userName).setValue(mUserFavorite)

        m_bFavoritedRestaurant = true
    }

    private fun unregisterFavoriteRestaurant() {

        mUserFavorite.favoriteRestaurants.remove(mRestaurant.documentId)

        mDatabaseReference!!.child("UserRating").child(mUserFavorite.userName).setValue(mUserFavorite)

        m_bFavoritedRestaurant = false
    }

    fun CreatePopUp() {

        // Initialize a new layout inflater instance
        val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        // Inflate a custom view using layout inflater
        val view = inflater.inflate(R.layout.popup_rating,null)

        // Initialize a new instance of popup window
        val popupWindow = PopupWindow(
                view, // Custom view to show in popup window
                LinearLayout.LayoutParams.WRAP_CONTENT, // Width of popup window
                LinearLayout.LayoutParams.WRAP_CONTENT // Window height
        )

        // Set an elevation for the popup window
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            popupWindow.elevation = 10.0F
        }


        // If API level 23 or higher then execute the code
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            // Create a new slide animation for popup window enter transition
            val slideIn = Slide()
            slideIn.slideEdge = Gravity.TOP
            popupWindow.enterTransition = slideIn

            // Slide animation for popup window exit transition
            val slideOut = Slide()
            slideOut.slideEdge = Gravity.RIGHT
            popupWindow.exitTransition = slideOut

        }

        // Get the widgets reference from custom view
        val buttonPopup = view.findViewById<Button>(R.id.bt_ok)
        val ratingBar = view.findViewById<RatingBar>(R.id.rb_selector)
        // Set a click listener for popup's button widget
        buttonPopup.setOnClickListener{
            mFragment!!.updateRating(ratingBar.rating)
            popupWindow.dismiss()
        }


        // Finally, show the popup window on app
        TransitionManager.beginDelayedTransition(restaurant_detail_container)
        popupWindow.showAtLocation(
                restaurant_detail_container, // Location to display popup window
                Gravity.CENTER, // Exact position of layout to display popup
                0, // X offset
                0 // Y offset
        )
    }

    companion object {
        /**
         * for extra definitions
         */
    }
}
