package co.moma.latinbites

import android.Manifest
import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.pm.PackageManager.GET_META_DATA
import android.location.Geocoder
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.ResultReceiver
import android.support.design.widget.BottomNavigationView
import android.support.design.widget.NavigationView
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.transition.Slide
import android.transition.TransitionManager
import android.util.Log
import android.view.*
import android.widget.*
import co.moma.latinbites.model.Constants
import co.moma.latinbites.model.UserFavorite
import co.moma.latinbites.utils.BottomNavigationHelper
import co.moma.latinbites.utils.FetchAddress
import co.moma.latinbites.utils.LocaleManager
import co.moma.latinbites.utils.Utility
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.base_layout.*
import kotlinx.android.synthetic.main.content_home.*
import kotlinx.android.synthetic.main.location_selector.*


open class BaseActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener{

    lateinit var mAuth:FirebaseAuth
    private var TAG = "BaseActivity"

    var mFusedLocationClient: FusedLocationProviderClient? = null
    private var lastLocation: Location? = null
    private var resultReceiver: AddressResultReceiver? = null
    private var addressOutput: String ? = null
    private val REQUEST_PERMISSIONS_REQUEST_CODE = 34
    private var et_location: EditText? = null
    var mFirebaseDatabase: FirebaseDatabase? = null
    var mDatabaseReference: DatabaseReference? = null
    var mUserFavorite: UserFavorite = UserFavorite()

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                startActivity(Intent(this, Main::class.java))
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_restaurants -> {
                startActivity(Intent(this, RestaurantActivity::class.java))
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_events -> {
                startActivity(Intent(this, EventActivity::class.java))
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_favorites -> {
                startActivity(Intent(this, FavoriteActivity::class.java))
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }
    internal inner class AddressResultReceiver(handler: Handler) : ResultReceiver(handler) {

        override fun onReceiveResult(resultCode: Int, resultData: Bundle?) {
            addressOutput = resultData?.getString(Constants.RESULT_DATA_KEY)

            if(resultCode == Constants.SUCCESS_RESULT){
                et_location!!.setText(addressOutput)
            }
            else {
                Utility.showSnackbar("Could not get your location", findViewById(R.id.rl_startup))
            }
        }
    }

    private fun resetTitle() {
        try {
            val label = packageManager.getActivityInfo(componentName, GET_META_DATA).labelRes;
            if (label != 0) {
                setTitle(label);
            }
        } catch (e: PackageManager.NameNotFoundException) {  }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        super.setContentView(R.layout.base_layout)
        setSupportActionBar(tb_base)
        resetTitle()

        mAuth = FirebaseAuth.getInstance()

        resultReceiver = AddressResultReceiver(Handler())

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        mFirebaseDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mFirebaseDatabase!!.reference

        if(mAuth.currentUser != null) {
            mUserFavorite.userName = mAuth.currentUser!!.uid
        }

        //This removes the animation from the bottom navigation bar
        BottomNavigationHelper.removeShiftMode(bnv_base)

        val toggle = ActionBarDrawerToggle(
                this, dl_base, tb_base, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        dl_base.addDrawerListener(toggle)
        toggle.syncState()

        nv_base.setNavigationItemSelectedListener(this)

        bnv_base.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }

    override fun onStart() {
        super.onStart()

        if (!Utility.checkPermissions(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            requestPermissions();
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_language -> {
                createLanguagesPopUp()
            }
            R.id.nav_location -> {
                createLocationPopUp()
            }
            R.id.nav_user_account -> {
                when (mAuth.currentUser != null) {
                    true -> {
                        val intent = Intent(this, UserSettings::class.java)
                        startActivity(intent)
                    }
                    false -> {
                        val intent = Intent(this, LoginActivity::class.java)
                        startActivity(intent)
                    }
                }
            }
        }
        dl_base.closeDrawer(GravityCompat.START)
        return true
    }

    override fun setContentView(layoutResID: Int) {
        if (frame != null) {
            val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val lp = ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT)
            val stubView = inflater.inflate(layoutResID, frame, false)
            frame.addView(stubView, lp)
        }
    }

    fun setCheckedItem(index: Int) {
        bnv_base.menu.getItem(index).setChecked(true)
    }
    fun goHome() {
        val intent = Intent(this, Main::class.java)
        startActivity(intent)
    }

    fun createLocationPopUp() {
        // Initialize a new layout inflater instance
        val inflater:LayoutInflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        // Inflate a custom view using layout inflater
        val view = inflater.inflate(R.layout.location_selector,null)

        // Initialize a new instance of popup window
        val popupWindow = PopupWindow(
                view, // Custom view to show in popup window
                LinearLayout.LayoutParams.WRAP_CONTENT, // Width of popup window
                LinearLayout.LayoutParams.WRAP_CONTENT // Window height
        )

        // Set an elevation for the popup window
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            popupWindow.elevation = 10.0F
        }


        // If API level 23 or higher then execute the code
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            // Create a new slide animation for popup window enter transition
            val slideIn = Slide()
            slideIn.slideEdge = Gravity.TOP
            popupWindow.enterTransition = slideIn

            // Slide animation for popup window exit transition
            val slideOut = Slide()
            slideOut.slideEdge = Gravity.RIGHT
            popupWindow.exitTransition = slideOut

        }

        val bt_ok = view.findViewById<Button>(R.id.bt_ok)
        val bt_findme = view.findViewById<Button>(R.id.bt_find_me)
        et_location = view.findViewById(R.id.et_location)

        bt_ok.setOnClickListener{
            popupWindow.dismiss()
        }

        getAddress()

        bt_findme.setOnClickListener{
            if (!Utility.checkPermissions(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                requestPermissions();
            } else {
                getAddress();
            }
        }

        popupWindow.setOnDismissListener {
            goHome()
        }

        TransitionManager.beginDelayedTransition(cl_container)
        popupWindow.showAtLocation(
                cl_container,
                Gravity.CENTER,
                0,
                0
        )

        popupWindow.isFocusable = true
        popupWindow.update()
    }

    fun createLanguagesPopUp() {
        // Initialize a new layout inflater instance
        val inflater:LayoutInflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        // Inflate a custom view using layout inflater
        val view = inflater.inflate(R.layout.language_selector,null)

        // Initialize a new instance of popup window
        val popupWindow = PopupWindow(
                view, // Custom view to show in popup window
                LinearLayout.LayoutParams.WRAP_CONTENT, // Width of popup window
                LinearLayout.LayoutParams.WRAP_CONTENT // Window height
        )

        // Set an elevation for the popup window
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            popupWindow.elevation = 10.0F
        }


        // If API level 23 or higher then execute the code
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            // Create a new slide animation for popup window enter transition
            val slideIn = Slide()
            slideIn.slideEdge = Gravity.TOP
            popupWindow.enterTransition = slideIn

            // Slide animation for popup window exit transition
            val slideOut = Slide()
            slideOut.slideEdge = Gravity.RIGHT
            popupWindow.exitTransition = slideOut

        }

        // Get the widgets reference from custom view
        val buttonPopup = view.findViewById<Button>(R.id.button_popup)
        val spinnner = view.findViewById<Spinner>(R.id.sp_language)
        // Set a click listener for popup's button widget
        buttonPopup.setOnClickListener{
            val index = spinnner.selectedItemPosition
            if(index != 0){
                changeLocale(index)
            }
            popupWindow.dismiss()
        }

        // Set a dismiss listener for popup window
        popupWindow.setOnDismissListener {
            goHome()
        }


        // Finally, show the popup window on app
        TransitionManager.beginDelayedTransition(cl_container)
        popupWindow.showAtLocation(
                cl_container, // Location to display popup window
                Gravity.CENTER, // Exact position of layout to display popup
                0, // X offset
                0 // Y offset
        )
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(LocaleManager.onAttach(newBase))
    }

    fun showProgress(show: Boolean, view: View, progressBar: ProgressBar) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            val shortAnimTime = resources.getInteger(android.R.integer.config_shortAnimTime).toLong()

            view.visibility = if (show) View.GONE else View.VISIBLE
            view.animate()
                    .setDuration(shortAnimTime)
                    .alpha((if (show) 0 else 1).toFloat())
                    .setListener(object : AnimatorListenerAdapter() {
                        override fun onAnimationEnd(animation: Animator) {
                            view.visibility = if (show) View.GONE else View.VISIBLE
                        }
                    })

            progressBar.visibility = if (show) View.VISIBLE else View.GONE
            progressBar.animate()
                    .setDuration(shortAnimTime)
                    .alpha((if (show) 1 else 0).toFloat())
                    .setListener(object : AnimatorListenerAdapter() {
                        override fun onAnimationEnd(animation: Animator) {
                            progressBar.visibility = if (show) View.VISIBLE else View.GONE
                        }
                    })
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            view.visibility = if (show) View.VISIBLE else View.GONE
            progressBar.visibility = if (show) View.GONE else View.VISIBLE
        }
    }

    fun changeLocale(index: Int){
        var locale: String = ""

        when(index){
            1 -> {
                locale = ""
            }
            2 -> {
                locale = "es"
            }
            3 -> {
                locale = "fr"
            }
        }
        LocaleManager.setLocale(this, locale)
    }

    private fun getLocation() {
        val intent = Intent(this, FetchAddress::class.java)
        intent.putExtra(Constants.RECEIVER, resultReceiver)
        intent.putExtra(Constants.LOCATION_DATA_EXTRA, lastLocation)
        startService(intent)
    }

    private fun requestPermissions() {
        val shouldProvideRationale = ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_FINE_LOCATION)

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.")

            showSnackbar(R.string.permission_rationale, android.R.string.ok,
                    View.OnClickListener {
                        // Request permission
                        ActivityCompat.requestPermissions(this@BaseActivity,
                                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                                REQUEST_PERMISSIONS_REQUEST_CODE)
                    })

        } else {
            Log.i(TAG, "Requesting permission")
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    REQUEST_PERMISSIONS_REQUEST_CODE)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        Log.i(TAG, "onRequestPermissionResult")
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.size <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i(TAG, "User interaction was cancelled.")
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission granted.
                //getAddress()
            } else {
                // Permission denied.

                // Notify the user via a SnackBar that they have rejected a core permission for the
                // app, which makes the Activity useless. In a real app, core permissions would
                // typically be best requested during a welcome-screen flow.

                // Additionally, it is important to remember that a permission might have been
                // rejected without asking the user for permission (device policy or "Never ask
                // again" prompts). Therefore, a user interface affordance is typically implemented
                // when permissions are denied. Otherwise, your app could appear unresponsive to
                // touches or interactions which have required permissions.
                /*showSnackbar(R.string.permission_denied_explanation, R.string.settings,
                        View.OnClickListener {
                            // Build intent that displays the App settings screen.
                            val intent = Intent()
                            intent.action = ACTION_APPLICATION_DETAILS_SETTINGS
                            val uri = Uri.fromParts("package",
                                    BuildConfig.APPLICATION_ID, null)
                            intent.data = uri
                            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                            startActivity(intent)
                        })*/
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun getAddress() {
        mFusedLocationClient!!.lastLocation
                .addOnSuccessListener(this, OnSuccessListener<Location> { location ->
                    if (location == null) {
                        Log.w(TAG, "onSuccess:null")
                        return@OnSuccessListener
                    }
                    lastLocation = location

                    if(lastLocation != null){
                        getLocation()
                    }

                    // Determine whether a Geocoder is available.
                    if (!Geocoder.isPresent()) {
                        Utility.showSnackbar("No geocoder available", rl_startup)
                        return@OnSuccessListener
                    }

                    // If the user pressed the fetch address button before we had the location,
                    // this will be set to true indicating that we should kick off the intent
                    // service after fetching the location.
                    /*if (mAddressRequested) {
                        startIntentService()
                    }*/
                })
                .addOnFailureListener(this) { e -> Log.w(TAG, "getLastLocation:onFailure", e) }
    }

    private fun showSnackbar(mainTextStringId: Int, actionStringId: Int,
                             listener: View.OnClickListener) {
        Snackbar.make(findViewById(android.R.id.content),
                getString(mainTextStringId),
                Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(actionStringId), listener).show()
    }
}