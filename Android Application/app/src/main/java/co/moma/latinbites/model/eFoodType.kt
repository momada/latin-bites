package co.moma.latinbites.model

enum class eFoodType(){
    Vegan,
    Vegetarian,
    Kosher
}