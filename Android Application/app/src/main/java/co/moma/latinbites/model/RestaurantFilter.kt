package co.moma.latinbites.model

import java.io.Serializable

class RestaurantFilter : Serializable {
    var common: Int = 0
    var food: Int = 0
    var country: Int = 0

    constructor()

    constructor(common: Int, food: Int, country: Int) {
        this.common = common
        this.food = food
        this.country = country
    }
}