package co.moma.latinbites.model

import java.io.Serializable

class RestaurantComments : Serializable {
    var userName: String = ""
    var comment: String = ""

    constructor()

    constructor(userName: String, comment: String) {
        this.userName = userName
        this.comment = comment
    }
}