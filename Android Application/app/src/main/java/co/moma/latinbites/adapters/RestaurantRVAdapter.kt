package co.moma.latinbites.adapters


import android.content.Intent
import android.graphics.BitmapFactory
import android.location.Location
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import co.moma.latinbites.R
import co.moma.latinbites.activities.RestaurantDetailActivity
import co.moma.latinbites.fragments.RestaurantDetailFragment
import co.moma.latinbites.model.eCountry
import co.moma.latinbites.model.RestaurantFilter
import co.moma.latinbites.model.Restaurant
import co.moma.latinbites.model.UserFavorite
import com.bumptech.glide.Glide
import com.futuremind.recyclerviewfastscroll.SectionTitleProvider
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.model_restaurant_rv.view.*
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by mfigueroa on 3/18/2018.
 */
class RestaurantRVAdapter(private val parentActivity: AppCompatActivity,
                          private var restaurantList: ArrayList<Restaurant>,
                          private var userFavorite: UserFavorite,
                          private var bTrending: Boolean,
                          private val twoPane: Boolean) :
        RecyclerView.Adapter<RestaurantRVAdapter.ViewHolder>(), SectionTitleProvider{

    private var m_bFiltering = false
    private var filteredRestaurants = ArrayList<Restaurant>()
    private var mFirstUse = true
    private var TAG = "RestaurantRVAdapter"
    private val onClickListener: View.OnClickListener


    init {
        onClickListener = View.OnClickListener { v ->
            val item = v.tag as Restaurant
            if (twoPane) {
                val fragment = RestaurantDetailFragment().apply {
                    arguments = Bundle().apply {
                        putSerializable(RestaurantDetailFragment.ARG_RESTAURANT, item)
                        putSerializable(RestaurantDetailFragment.ARG_FAVORITES, userFavorite)
                    }
                }
                parentActivity.supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.restaurant_detail_container, fragment)
                        .commit()
            } else {
                val intent = Intent(v.context, RestaurantDetailActivity::class.java).apply {
                    putExtra(RestaurantDetailFragment.ARG_RESTAURANT, item)
                    putExtra(RestaurantDetailFragment.ARG_FAVORITES, userFavorite)
                }
                v.context.startActivity(intent)
            }
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val currentList = getCurrentList()

        holder.tvRestaurantName.text = currentList[position].name
        holder.tvRestaurantInfo.text = currentList[position].website
        holder.tvRestaurantRating.text = currentList[position].averageRating.toString()

        val sName = String.format("Restaurants/%s.jpg",
                currentList[position].name.toLowerCase().replace(" ",""))

        FirebaseStorage.getInstance().reference.child(sName).downloadUrl.addOnSuccessListener {it ->

            Glide.with(holder.ivRestaurant.context).load(it)
                    .into(holder.ivRestaurant)
        }.addOnFailureListener{
            Log.e(TAG, String.format("Source image not found: %s", it.message))
        }

        with(holder.itemView){
            tag = currentList[position]
            setOnClickListener(onClickListener)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater
                .from(parent.context)
                .inflate(R.layout.model_restaurant_rv, parent, false))
    }

    override fun onViewRecycled(holder: ViewHolder) {
        super.onViewRecycled(holder)
        holder.ivRestaurant.setImageBitmap(BitmapFactory.decodeResource(holder.itemView.context.resources, R.drawable.ic_latin_vibes))
    }

    override fun getItemCount(): Int {
        return when(m_bFiltering){
            true -> filteredRestaurants.size
            false -> restaurantList.size
        }
    }

    fun getCurrentRestaurantList(): ArrayList<Restaurant> {
        when(m_bFiltering){
            true -> return filteredRestaurants
            false -> return restaurantList
        }
    }

    fun canOpenMap(): Boolean {
        var bResult = false

        val currentList = getCurrentList()

        currentList.forEach { it ->
            it.franchises.forEach { it2 ->
                if(it2.longitude.toDouble() != 0.0 && it2.latitude.toDouble() != 0.0) {
                    bResult = true
                    return@forEach
                }
            }
        }

        return bResult
    }

    fun filterRestaurant(filter: RestaurantFilter) {
        m_bFiltering= true

        val sCountryFilter = eCountry.values()[filter.country].toString()

        val newRestaurants = restaurantList.filter {
            simpleRestaurant -> simpleRestaurant.country!! == sCountryFilter
            //|| simpleRestaurant.foodType.contains(eFoodType.values()[filter.food].name)
        } as ArrayList<Restaurant>

        if(newRestaurants.size == restaurantList.size && !mFirstUse) {
            DiffUtil.calculateDiff(PhotoRowDiffCallback(newRestaurants, filteredRestaurants), false).dispatchUpdatesTo(this)
        }
        else {
            DiffUtil.calculateDiff(PhotoRowDiffCallback(newRestaurants, restaurantList), false).dispatchUpdatesTo(this)
            mFirstUse = false
        }
        filteredRestaurants = newRestaurants
    }

    fun filterRestaurant(query: String) {
        m_bFiltering= true

        val newRestaurants = restaurantList.filter { simpleRestaurant ->
            simpleRestaurant.name.toLowerCase().contains(query.toLowerCase()) || simpleRestaurant.country!!.toLowerCase()!!.contains(query.toLowerCase())
            //|| simpleRestaurant.foodType.contains(query)
        } as ArrayList<Restaurant>

        if(newRestaurants.size == restaurantList.size && !mFirstUse) {
            DiffUtil.calculateDiff(PhotoRowDiffCallback(newRestaurants, filteredRestaurants), false).dispatchUpdatesTo(this)
        }
        else {
            DiffUtil.calculateDiff(PhotoRowDiffCallback(newRestaurants, restaurantList), false).dispatchUpdatesTo(this)
            mFirstUse = false
        }
        filteredRestaurants = newRestaurants
    }

    fun filterNearMe(i_location: Location) {
        val currentList = getCurrentList()
        var distanceList = defineDistance(i_location.latitude, i_location.longitude, currentList)
        var newList : ArrayList<Restaurant> = ArrayList()

        for( currentDistance in distanceList ){
            newList.add(currentList.find { it -> it.code == currentDistance.key }!!)
        }

        DiffUtil.calculateDiff(PhotoRowDiffCallback(newList, restaurantList), false).dispatchUpdatesTo(this)
        restaurantList = ArrayList(newList)
    }

    fun rad(number: Double) : Double {
        return number*Math.PI/180
    }

    fun defineDistance( dLatitude: Double, dLongitude: Double, currentList: ArrayList<Restaurant> ): SortedMap<Int, Double> {
        val lat = dLatitude
        val lng = dLongitude
        val R = 6371 // radius of earth in km
        val alDistances: HashMap<Int, Double> = HashMap()
        for (currentRestaurant in currentList) {
            var distanceSum = 0.0

            for(currentFranchise in currentRestaurant.franchises) {

                val mlat = currentFranchise.latitude.toDouble()
                val mlng = currentFranchise.longitude.toDouble()
                val dLat  = rad(mlat - lat)
                val dLong = rad(mlng - lng)
                val a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                        Math.cos(rad(lat)) * Math.cos(rad(lat)) * Math.sin(dLong/2) * Math.sin(dLong/2)
                val c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a))
                val d = R * c;
                distanceSum += d
            }

            alDistances[currentRestaurant.code] = distanceSum
        }

        return alDistances.toSortedMap()
    }

    class PhotoRowDiffCallback(private val newRows : List<Restaurant>, private val oldRows : List<Restaurant>) : DiffUtil.Callback() {
        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val oldRow = oldRows[oldItemPosition]
            val newRow = newRows[newItemPosition]
            return oldRow == newRow
        }

        override fun getOldListSize(): Int = oldRows.size

        override fun getNewListSize(): Int = newRows.size

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val oldRow = oldRows[oldItemPosition]
            val newRow = newRows[newItemPosition]
            return oldRow == newRow
        }
    }

    override fun getSectionTitle(position: Int): String {
        var title = "";
        try {
            title = getCurrentList()[position].name.substring(0, 1)
        }
        catch (ex: Exception)
        {
            Log.e(TAG,"Something went wrong retrieving the title")
        }
        return title
    }

    fun getCurrentList() : ArrayList<Restaurant> {
        var currentList = ArrayList<Restaurant>()

        if(m_bFiltering) {
            currentList = filteredRestaurants
        }
        else {
            currentList = restaurantList
        }
        return currentList
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val tvRestaurantName: TextView = itemView.tv_restaurant_name
        val tvRestaurantInfo: TextView = itemView.tv_restaurant_info
        val tvRestaurantRating: TextView = itemView.tv_restaurant_rating
        val ivRestaurant: ImageView = itemView.iv_restaurant
    }

}