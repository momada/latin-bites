package co.moma.latinbites.model

import java.io.Serializable

class FoodType : Serializable {

    var food: String ? = null
    constructor() {}

    constructor(food: String?) {
        this.food = food
    }
}