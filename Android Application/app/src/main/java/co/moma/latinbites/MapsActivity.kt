package co.moma.latinbites

import android.annotation.SuppressLint
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import co.moma.latinbites.model.Franchise
import co.moma.latinbites.model.Restaurant
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.MarkerOptions

@Suppress("UNCHECKED_CAST")
class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private var mapBounds: LatLngBounds ? = null
    private val latLngBuilder = LatLngBounds.builder()

    private var alRestaurants = ArrayList<Restaurant>()
    private var oFranchise = Franchise()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)

        if( intent.hasExtra("alRestaurants") ){
            alRestaurants = intent.getSerializableExtra("alRestaurants") as ArrayList<Restaurant>
        }
        else if (intent.hasExtra("Franchise")) {
            oFranchise = intent.getSerializableExtra("Franchise") as Franchise
        }
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map2) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @SuppressLint("MissingPermission")
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        mMap.setOnMapLoadedCallback {
            mapBounds = latLngBuilder.build()
            mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(mapBounds, 0))
            mMap.moveCamera(CameraUpdateFactory.zoomTo(10.0f))
            mMap.isMyLocationEnabled = true

        }

        if(alRestaurants.size > 0){
            alRestaurants.forEach { it ->

                it.franchises.forEach { it2 ->
                    if(it2.longitude.toDouble() != 0.0 && it2.latitude.toDouble() != 0.0) {

                        val mMarker = LatLng(it2.longitude.toDouble(), it2.latitude.toDouble())
                        mMap.addMarker(MarkerOptions().position(mMarker).title("${it.name} at ${it2.location}"))
                        latLngBuilder.include(mMarker)
                    }
                }
            }
        }
        else{
            if(oFranchise.longitude.toDouble() != 0.0 && oFranchise.latitude.toDouble() != 0.0) {
                val mMarker = LatLng(oFranchise.longitude.toDouble(), oFranchise.latitude.toDouble())
                mMap.addMarker(MarkerOptions().position(mMarker).title(oFranchise.location))
                latLngBuilder.include(mMarker)
            }

        }

    }

}
