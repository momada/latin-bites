package co.moma.latinbites.model

import java.io.Serializable

class UserFavorite: Serializable {

    var userName: String = ""
    var favoriteRestaurants: ArrayList<String> = ArrayList()

    constructor()

    constructor(userName: String, favoriteRestaurants: ArrayList<String>) {
        this.userName = userName
        this.favoriteRestaurants = favoriteRestaurants
    }
}