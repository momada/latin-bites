package co.moma.latinbites.model

/**
 * Created by mfigueroa on 3/18/2018.
 */
class User {
    private var firstName = ""
    private var lastName = ""
    private var email = ""
    private var language = 0

    fun User(firstName: String, lastName: String, email: String) {
        this.firstName = firstName
        this.lastName = lastName
        this.email = email
        this.language = 0
    }

    fun getDisplayName() : String{
        return this.firstName + " " + this.lastName
    }

    fun getEmail() : String {
        return this.email
    }

    fun getPreferredLanguage() : Int {
        return this.language
    }
}