package co.moma.latinbites.utils

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.view.View

class Utility {

    companion object {
        fun showSnackbar(text: String, view: View) {
            val container = view.findViewById<View>(android.R.id.content)
            if (container != null) {
                Snackbar.make(container, text, Snackbar.LENGTH_LONG).show()
            }
        }
        fun checkPermissions(context: Context, sPermissionName: String): Boolean {
            val permissionState = ActivityCompat.checkSelfPermission(context,
                    sPermissionName)
            return permissionState == PackageManager.PERMISSION_GRANTED
        }
    }
}