package co.moma.latinbites.model

/**
 * Created by mfigueroa on 4/9/2018.
 */
data class Card(val productImage: Int, val productName: String)