package co.moma.latinbites.model

enum class eLanguages(val index: Int) {
    English(1){
        val desc = ""
    },
    Spanish(2) {
        val desc = "es"
    },
    French(3) {
        val desc = "fr"
    },
}