var async = require("async")
var admin = require("firebase-admin");
var firebase = require("firebase");

var serviceAccount = require("C:/Work/LatinVibes/Firestore/LatinVibesKey.json");

admin.initializeApp({
credential: admin.credential.cert(serviceAccount),
databaseURL: "https://latin-bites.firebaseio.com"
});

var db = admin.firestore();

var sql = require("mssql");

var config = {
    user: 'LvAdmin',
    password: 'lv2019',
    server: 'localhost',
    database: 'LatinVibes',
    port: 1434
};

var realtimeConfig = {
    apiKey: "AIzaSyCHN3sSibPQcKdCcVPTnxdZbSKjBxwI1_M",
    authDomain: "latin-bites.firebaseapp.com",
    databaseURL: "https://latin-bites.firebaseio.com",
    projectId: "latin-bites",
    storageBucket: "latin-bites.appspot.com",
    messagingSenderId: "871994563121"
}

firebase.initializeApp(realtimeConfig);

var realtimeDB = firebase.database();

var Restaurant = {
    code: Intl,
    name: String,
    website: String,
    enabled: Boolean,
    country: String,
    logo: String,
    foodType: Array,
    franchises: Array,
    averageRating: Number,
    payment: Array,
    trending: Intl,
};

var Payment = {
    cardBrand: String,
    entryMethod: String
};

var FoodType= {
    food: String
};

var Franchise = {
    id: Intl,
    location: String,
    address: String,
    number: String,
    delivery: Boolean,
    longitude: String,
    latitude: String,
    schedule: Array,
    //rating and comment?
};

var Schedule = {
    franchiseAddress: Intl,
    openDays: String,
    openingTime: Date,
    closingTime: Date
};

var Comments = {
    franchiseAddress: Intl,
    comments: String,
    rating: Intl,
    userName: String,
    dateTime: Date
};



getRestaurants(function (err, rest){
    if(err){
        console.log(err);
        return;
    }

    //console.log(rest);
    sql.close();
    getRestaurantInfo(rest, function(err, resto) {
        if(err) {
            console.log(err);
            return;
        }

        //console.log(resto);
        sql.close();
    })
})

function getRestaurants(getRestaurantCB){
    
    sql.connect(config, function(err) {

        if(err) {
            console.log("Connection error ", err);
            getRestaurantCB(err);
            return;
        }

        console.log("Connected to databasse");

        var request = new sql.Request();

        request.execute('sp_GetRestaurants')
        .then( rows => {
            console.log("getRestaurant done");
            getRestaurantCB(null, rows);
        })
    })    
}

function getRestaurantInfo(restaurants, getRestaurantInfoCB) {

    sql.connect(config, function(err) {

        if(err) {
            console.log("Connection error ", err);
            getRestaurantInfoCB(err);
            return;
        }

        var restaurantInfo = new Array();
        async.eachSeries(restaurants.recordset, function (id, cb) {
            setTimeout(function() {

                Restaurant = {}
                var restaurantInfo = new Array();
                var foodTypeArray = new Array();
                var scheduleArray = new Array();
                var paymentArray = new Array();

                Restaurant.code = id.id_restaurant;
                Restaurant.name = id.restaurant_name;
                Restaurant.website = id.restaurant_website;
                Restaurant.enabled = id.enabled;
                Restaurant.logo = id.logo;
                Restaurant.country = id.country_name;
                Restaurant.trending = id.trending;
                Restaurant.averageRating = id.restaurant_avg;

                var paymentRequest = new sql.Request();
                paymentRequest.input('restaurant_id', id.id_restaurant);
                paymentRequest.execute('Sp_getPayMethod')
                .then(pm => {
                    pm.recordset.forEach(function(paymentMethod){
                        Payment = {}
                        Payment.cardBrand = paymentMethod.brand_desc;
                        Payment.entryMethod = paymentMethod.entry_desc;
                        paymentArray.push(Payment);
                    })
                })
                .then(() => {
                    var foodTypeRequest = new sql.Request();
                    foodTypeRequest.input('restaurant_id', id.id_restaurant);
                    foodTypeRequest.execute('Sp_getFoodType')
                    .then(ft => {
                        ft.recordset.forEach(function (foodType) {
                            FoodType = {}
                            FoodType.food = foodType.type_desc;
                            //console.log(foodType.type_desc)
                            foodTypeArray.push(FoodType);
                        })
                    })
                    .then(() => {
                        //console.log(foodTypeArray)
                        
                        var scheduleRequest = new sql.Request();
                        scheduleRequest.input('restaurant_id', id.id_restaurant);
                        scheduleRequest.execute('Sp_getSchedule')
                        .then(sh => {
                            sh.recordset.forEach(function(schedule){
                                Schedule = {};
                                        
                                Schedule.franchiseAddress = schedule.id_address
                                Schedule.openDays = schedule.open_days;
                                Schedule.openingTime = schedule.opening_time;
                                Schedule.closingTime = schedule.closing_time;
                                
                                scheduleArray.push(Schedule);
                                //console.log(id.id_restaurant)
                                //console.log(scheduleArray)
                            })
                        })
                        .then(()=> {
                                    
                            var infoRequest = new sql.Request();
                            infoRequest.input('restaurant_id', id.id_restaurant);
                            infoRequest.execute('sp_GetRestaurantInfo')
                            .then( ri => {
                                    ri.recordset.forEach( function(info) {
                                    Franchise = {};
                                    
                                    Franchise.id = info.id_address;
                                    Franchise.location = info.location_desc;
                                    Franchise.address = info.address_desc;
                                    Franchise.number = info.phone_number;
                                    Franchise.delivery = info.yn_delivery;
                                    Franchise.longitude = info.longitude;
                                    Franchise.latitude = info.latitude;
                                    Franchise.schedule = new Array(scheduleArray[info.id_address-1]);
                                    restaurantInfo.push(Franchise);
                                });
        
                    })
                    .then(() => {
                        Restaurant.franchises = restaurantInfo;
                        Restaurant.foodType = foodTypeArray;
                        Restaurant.payment = paymentArray;
                        console.log(JSON.stringify(Restaurant));
                        realtimeDB.ref("Restaurants").push(Restaurant);
                        //db.collection('Restaurant').doc().set(Restaurant)
                        console.log("listo")
                        return cb();
                        })
                    })
                    })
                })                
            }), 1000000;
        }, function (err) {
            console.log("Final call");
            getRestaurantInfoCB(null, Restaurant);  
            });
    })
}