
drop procedure sp_getSimpleRestaurant;
drop procedure sp_getRestaurant;
drop procedure sp_getRestaurantInfo;
drop procedure sp_getRestaurantRating;
drop procedure sp_getRestaurantByFood;
drop procedure sp_getRestaurantByCountry;

delimiter //
create procedure sp_getSimpleRestaurant()
begin
	select  r.id_restaurant,
			ld.id_location,
			r.restaurant_name,
			(select avg(rating)
            from rating 
            where id_restaurant = r.id_restaurant
            and id_location = ri.id_location) as avg_rating,
			ld.location_desc
    from restaurant r
    left join restaurant_info ri on r.id_restaurant = ri.id_restaurant
    left join location_desc ld on ri.id_location = ld.id_location
    where r.enabled = 1;
end //

delimiter //
create procedure sp_getRestaurant()
begin
	select id_restaurant,
		   restaurant_name
	from restaurant
	where enabled = 1;
end //

delimiter //
create procedure sp_getRestaurantInfo()
begin
	select  r.id_restaurant,
            ld.id_location,
			ld.location_desc,
			c.country_name,
            ri.address_desc, 
            ri.phone_number, 
            rd.opening_time, 
            rd.closing_time, 
            rd.open_days, 
			(select avg(rating)
            from rating 
            where id_restaurant = r.id_restaurant
            and id_location = ri.id_location) as avg_rating,
            ft.type_desc,
            ri.yn_delivery,
            em.entry_desc
    from restaurant r
    left join restaurant_info ri on r.id_restaurant = ri.id_restaurant
    left join country c on ri.id_country = c.id_country
    left join location_desc ld on ri.id_location = ld.id_location
    left join restaurant_pay_info rpi on ri.id_restaurant = rpi.id_restaurant and ri.id_location = rpi.id_location
    left join entry_method em on rpi.id_entry = em.id_entry and em.enabled = 1
    left join restaurant_food_type rft on r.id_restaurant = rft.id_restaurant and ri.id_location = rft.id_location
    left join food_types ft on rft.id_type = ft.id_type
    left join restaurant_days rd on r.id_restaurant = rd.id_restaurant and ri.id_address = rd.id_address
    where r.enabled = 1;
end //

delimiter //
create procedure sp_getRestaurantRating()
begin
	select ra.user_name, ra.rating, comment_desc 
    from comments c
    left join rating ra on c.user_name = ra.user_name and c.id_restaurant = ra.id_restaurant
    left join restaurant r on c.id_restaurant = r.id_restaurant 
    left join restaurant_info ri on r.id_restaurant = ri.id_restaurant and ra.id_location = ri.id_location
    where r.enabled = 1;
end //

delimiter //
create procedure sp_getRestaurantByFood()
begin 
	select r.id_restaurant,
		   ri.id_location,
           ft.type_desc
	from restaurant r
    left join restaurant_info ri on r.id_restaurant = ri.id_restaurant
	left join restaurant_food_type rft on r.id_restaurant = rft.id_restaurant and ri.id_location = rft.id_location
    left join food_types ft on rft.id_type = ft.id_type
    where r.enabled = 1;
end //

delimiter //
create procedure sp_getRestaurantByCountry()
begin 
	select r.id_restaurant,
		   c.country_name
	from restaurant r
    left join restaurant_info ri on r.id_restaurant = ri.id_restaurant
	left join country c on ri.id_country = c.id_country
    where r.enabled = 1;
end //

