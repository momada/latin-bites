USE [master]
GO

/* For security reasons the login is created disabled and with a random password. */
/****** Object:  Login [LvAdmin]    Script Date: 5/12/2019 12:16:49 a. m. ******/
CREATE LOGIN [LvAdmin] WITH PASSWORD=N'+ymIi5lCxfqes8+fAVNg7jiOGo2rRHqhlve0q7FtcJE=', DEFAULT_DATABASE=[master], DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=ON
GO

ALTER LOGIN [LvAdmin] DISABLE
GO

ALTER SERVER ROLE [sysadmin] ADD MEMBER [LvAdmin]
GO


