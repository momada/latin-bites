--drop procedure Sp_getrestaurantinfo
--GO
--drop procedure sp_GetRestaurants
--GO
--drop procedure Sp_getComments
--GO
--drop procedure Sp_getPayMethod
--GO
--drop procedure Sp_getSchedule
--GO
--drop procedure Sp_getFoodType
--GO


CREATE PROCEDURE sp_GetRestaurants
AS
SELECT R.id_restaurant,
	   R.restaurant_name,
	   R.restaurant_website,
	   R.logo,
	   R.enabled, 
       c.country_name,
	   (select isnull(avg(cast(rating as float)),0)
	    from rating
		where id_restaurant = R.id_restaurant) as restaurant_avg,
	   R.trending
FROM   restaurant R 
       INNER JOIN restaurant_info RI ON r.id_restaurant = RI.id_restaurant 
       INNER JOIN country C ON c.id_country = ri.id_country and ri.id_address = 1
	   where R.enabled = 1
GO

CREATE PROCEDURE Sp_getrestaurantinfo @restaurant_id INT 
AS 

    SELECT RI.id_address, 
           LD.location_desc, 
           RI.address_desc, 
           RI.phone_number, 
           RI.yn_delivery, 
           cast(ISNULL(RI.longitude, 0) AS varchar) As longitude,  
           cast(ISNULL(RI.latitude, 0) AS varchar) As latitude 
    FROM   restaurant_info RI 
           INNER JOIN location_desc LD 
                   ON RI.id_location = LD.id_location 
    WHERE  id_restaurant = @restaurant_id 

go 


CREATE PROCEDURE Sp_getComments(@restaurant_id INT)
AS 

	SELECT RA.id_address,
		   RA.comment_desc,
		   RA.rating,
		   RA.user_name,
		   RA.date_time
	FROM restaurant  R
	INNER JOIN restaurant_info RI ON R.id_restaurant = RI.id_restaurant
	INNER JOIN rating RA ON R.id_restaurant = RA.id_restaurant and RI.id_address = RA.id_address
	WHERE RA.id_restaurant = @restaurant_id
go 


CREATE PROCEDURE Sp_getPayMethod(@restaurant_id INT)
AS 
	DECLARE @Brand Varchar(30), @Entry Varchar(30)
	
	IF( SELECT COUNT(*) FROM restaurant_pay_info where id_restaurant = @restaurant_id) > 0
	BEGIN

	SELECT @Brand = CB.brand_desc,
		   @Entry = EM.entry_desc
	FROM restaurant R 
	INNER JOIN restaurant_info RI ON R.id_restaurant = RI.id_restaurant
	INNER JOIN restaurant_pay_info RPI ON R.id_restaurant = RPI.id_restaurant and RI.id_address = RPI.id_address
	INNER JOIN card_brand CB ON RPI.id_brand = CB.id_brand AND CB.enabled = 1
	INNER JOIN entry_method EM ON RPI.id_entry = EM.id_entry AND EM.enabled = 1
	WHERE RPI.id_restaurant = @restaurant_id
	END
	ELSE
	BEGIN
		SET @Brand = 'None'
		SET	@Entry = 'None'
	END
	SELECT ISNULL(@Brand, 'None') As brand_desc,
		   ISNULL(@Entry, 'None') As entry_desc
go

CREATE PROCEDURE Sp_getSchedule(@restaurant_id INT)
AS 
		SELECT RI.id_address,
			   ISNULL(RD.open_days, 'None') As open_days, 
			   ISNULL(RD.opening_time, '00:00:00') As opening_time, 
			   ISNULL(RD.closing_time, '00:00:00') As closing_time 
		FROM restaurant_info RI
		LEFT JOIN restaurant_days RD ON RI.id_restaurant = RD.id_restaurant AND RI.id_address = RD.id_address
		WHERE RI.id_restaurant = @restaurant_id
	go
	
CREATE PROCEDURE Sp_getFoodType(@restaurant_id INT)
AS 
	DECLARE @FoodType VARCHAR(10)

	IF( SELECT COUNT(*) FROM restaurant_food_type where id_restaurant = @restaurant_id) > 0
	BEGIN
		SELECT @FoodType =  FT.type_desc
		FROM food_types FT
		left JOIN restaurant_food_type RFT ON FT.id_type = RFT.id_type
		WHERE RFT.id_restaurant = @restaurant_id
	END
	ELSE
		SET @FoodType = 'None'

	SELECT @FoodType As type_desc
go
