/*delete from restaurant_food_type;
delete from food_types;
delete from rating;
delete from restaurant_pay_info;
delete from entry_method;
delete from card_brand;
delete from restaurant_days;
delete from restaurant_info;
delete from location_desc;
delete from restaurant_info;
delete from country;
delete from restaurant;*/


INSERT INTO restaurant VALUES(1, '3 Amigos Resto Bar','http://www.3amigosrestaurant.com', 1, '',1);
INSERT INTO restaurant VALUES(2, 'Acajou Br', '', 1, '',0);
INSERT INTO restaurant VALUES(3, 'Agave', 'http://www.restaurantagave.com', 1, '',0);
INSERT INTO restaurant VALUES(4, 'Amaranto', '', 1, '',0);
INSERT INTO restaurant VALUES(5, 'Amigos Cantina Grill', 'http://www.amigoscantona.ca', 1, '',0);
INSERT INTO restaurant VALUES(6, 'Amour a la Mexicaine', '', 1, '',0);
INSERT INTO restaurant VALUES(7, 'Arepera du Plateau', '', 1, '',0);
INSERT INTO restaurant VALUES(8, 'Arriba Burrito', '', 1, '',0);
INSERT INTO restaurant VALUES(9, 'Augusto Al Gusto', '', 1, '',0);
INSERT INTO restaurant VALUES(10, 'Barraca Rhumeria Montreal', 'http://www.barraca.ca', 1, '',0);
INSERT INTO restaurant VALUES(11, 'Barros Luco', '', 1, '',0);
INSERT INTO restaurant VALUES(12, 'Bistro Carmel', '', 1, '',0);
INSERT INTO restaurant VALUES(13, 'Bistro Lomito''s', '', 1, '',0);
INSERT INTO restaurant VALUES(14, 'Bocadillo', '', 1, '',0);
INSERT INTO restaurant VALUES(15, 'Bocadillo Bistro', '', 1, '',0);
INSERT INTO restaurant VALUES(16, 'Burrito Borracho', '', 1, '',0);
INSERT INTO restaurant VALUES(17, 'Burrito Revolucion', 'http://www.burritorevolucion.ca', 1, '',0);
INSERT INTO restaurant VALUES(18, 'Burrito Shop', '', 1, '',0);
INSERT INTO restaurant VALUES(19, 'Cabanas Pupuseria', '', 1, '',0);
INSERT INTO restaurant VALUES(20, 'Cachitos', '', 1, '',0);
INSERT INTO restaurant VALUES(21, 'Cacique Grill', '', 1, '',0);
INSERT INTO restaurant VALUES(22, 'Cactus', '', 1, '',0);
INSERT INTO restaurant VALUES(23, 'Café Cantina', '', 1, '',0);
INSERT INTO restaurant VALUES(24, 'Café Frida', '', 1, '',0);
INSERT INTO restaurant VALUES(25, 'Café Mezcal', '', 1, '',0);
INSERT INTO restaurant VALUES(26, 'Cafe Net-Work', 'http://www.net-workcafe.com', 1, '',0);
INSERT INTO restaurant VALUES(27, 'Caifan', '', 1, '',0);
INSERT INTO restaurant VALUES(28, 'Calaveras', '', 1, '',0);
INSERT INTO restaurant VALUES(29, 'Callao', '', 1, '',0);
INSERT INTO restaurant VALUES(30, 'Carlos & Pepes', '', 1, '',0);
INSERT INTO restaurant VALUES(31, 'Casa Figueroa', 'http://www.casafigueroa.ca', 1, '',0);
INSERT INTO restaurant VALUES(32, 'Casa Munayco', '', 1, '',0);
INSERT INTO restaurant VALUES(33, 'Chez Frida', '', 1, '',0);
INSERT INTO restaurant VALUES(34, 'Chez Tina Gourmet', '', 1, '',0);
INSERT INTO restaurant VALUES(35, 'Chicho''s', 'http://www.restaurantchichos.ca', 1, '',0);
INSERT INTO restaurant VALUES(36, 'Chipotle & Jalapeno', '', 1, '',0);
INSERT INTO restaurant VALUES(37, 'Churros Montreal', '', 1, '',3);
INSERT INTO restaurant VALUES(38, 'Cuba Paisa', '', 1, '',0);
INSERT INTO restaurant VALUES(39, 'Don Taco', '', 1, '',0);
INSERT INTO restaurant VALUES(40, 'DoughBoys Pizza & Tacos', '', 1, '',0);
INSERT INTO restaurant VALUES(41, 'Eche Pa''Echarle Pena', '', 1, '',0);
INSERT INTO restaurant VALUES(42, 'El 77', '', 1, '',0);
INSERT INTO restaurant VALUES(43, 'El Amigo', '', 1, '',0);
INSERT INTO restaurant VALUES(44, 'El Chalateco', '', 1, '',0);
INSERT INTO restaurant VALUES(45, 'El Gordo', '', 1, '',0);
INSERT INTO restaurant VALUES(46, 'El Jibaro', '', 1, '',0);
INSERT INTO restaurant VALUES(47, 'El Meson', 'http://www.restaurantelmeson.ca', 1, '',0);
INSERT INTO restaurant VALUES(48, 'El rey del Taco (Le Roi du Taco)', '', 1, '',0);
INSERT INTO restaurant VALUES(49, 'El Sabor de Mexico', '', 1, '',0);
INSERT INTO restaurant VALUES(50, 'El Sabor de mi Tierra', '', 1, '',0);
INSERT INTO restaurant VALUES(51, 'El Zaziummm', 'http://www.elzaziummmrestaurant.com', 1, '',0);
INSERT INTO restaurant VALUES(52, 'Escape', '', 1, '',0);
INSERT INTO restaurant VALUES(53, 'Escondite', ' www.escondite.ca', 1, '',0);
INSERT INTO restaurant VALUES(54, 'Fortune', '', 1, '',0);
INSERT INTO restaurant VALUES(55, 'Grumman''78', '', 1, '',0);
INSERT INTO restaurant VALUES(56, 'Guanabara', '', 1, '',0);
INSERT INTO restaurant VALUES(57, 'Icehouse', '', 1, '',0);
INSERT INTO restaurant VALUES(58, 'Iguanas Ranas', '', 1, '',0);
INSERT INTO restaurant VALUES(59, 'Iris', '', 1, '',0);
INSERT INTO restaurant VALUES(60, 'Joverse', 'www.joverse.com', 1, '',0);
INSERT INTO restaurant VALUES(61, 'Julieta Cuisine Latine', '', 1, '',0);
INSERT INTO restaurant VALUES(62, 'Kathy & Kimy', '', 1, '',0);
INSERT INTO restaurant VALUES(63, 'La Bodeguita de Montreal', '', 1, '',0);
INSERT INTO restaurant VALUES(64, 'La Capital Tacos', '', 1, '',0);
INSERT INTO restaurant VALUES(65, 'La Carreta', '', 1, '',0);
INSERT INTO restaurant VALUES(66, 'La Cecilia', '', 1, '',0);
INSERT INTO restaurant VALUES(67, 'La Chilenita de la Montagne', '', 1, '',0);
INSERT INTO restaurant VALUES(68, 'La Comida', '', 1, '',0);
INSERT INTO restaurant VALUES(69, 'La Guadalupe Mexicaine', 'http://laguadalupemexicaine.com', 1, '',0);
INSERT INTO restaurant VALUES(70, 'La Guanaquita', '', 1, '',0);
INSERT INTO restaurant VALUES(71, 'La Habanera Café Cubano', 'http://www.lahabanera.ca/', 1, '',0);
INSERT INTO restaurant VALUES(72, 'La Selva', 'http://www.restaurantlaselva.com', 1, '',0);
INSERT INTO restaurant VALUES(73, 'La Tamalera', '', 1, '',0);
INSERT INTO restaurant VALUES(74, 'Larimar', '', 1, '',5);
INSERT INTO restaurant VALUES(75, 'Las Tres Conchitas', '', 1, '',0);
INSERT INTO restaurant VALUES(76, 'L''Atelier d''Argentine', ' www.atelierargentine.com', 1, '',0);
INSERT INTO restaurant VALUES(77, 'Lavanderia', '', 1, '',0);
INSERT INTO restaurant VALUES(78, 'Le Bon Vivant', 'http://www.lebv.ca', 1, '',0);
INSERT INTO restaurant VALUES(79, 'Le Café by Aldoria', '', 1, '',0);
INSERT INTO restaurant VALUES(80, 'Le Ice & Tacos', '', 1, '',0);
INSERT INTO restaurant VALUES(81, 'Le Milsa', 'http://www.lemilsa.com', 1, '',0);
INSERT INTO restaurant VALUES(82, 'Le Perche Bar Terrasse', 'http://perchemtl.com', 1, '',0);
INSERT INTO restaurant VALUES(83, 'Le Petite Coin du Mexique', '', 1, '',0);
INSERT INTO restaurant VALUES(84, 'Le Tequila Bar', '', 1, '',0);
INSERT INTO restaurant VALUES(85, 'Le Tex Mex', '', 1, '',0);
INSERT INTO restaurant VALUES(86, 'Les Bouchees Pasa palos', '', 1, '',0);
INSERT INTO restaurant VALUES(87, 'Les Deux Fours', '', 1, '',0);
INSERT INTO restaurant VALUES(88, 'Les Empanadas Pachamama', 'http://www.pachas.ca', 1, '',0);
INSERT INTO restaurant VALUES(89, 'Los Planes Gourmet', '', 1, '',0);
INSERT INTO restaurant VALUES(90, 'Los Planes Traditionnel', '', 1, '',0);
INSERT INTO restaurant VALUES(91, 'M. Burrito', '', 1, '',0);
INSERT INTO restaurant VALUES(92, 'M4 Burritos', '', 1, '',0);
INSERT INTO restaurant VALUES(93, 'Mademoiselle Urban RestoBar', '', 1, '',0);
INSERT INTO restaurant VALUES(94, 'Madre', 'http://www.restaurantmadre.com/', 1, '',0);
INSERT INTO restaurant VALUES(95, 'Mais', '', 1, '',0);
INSERT INTO restaurant VALUES(96, 'Maison Tequila Taco House', '', 1, '',0);
INSERT INTO restaurant VALUES(97, 'Manana', '', 1, '',0);
INSERT INTO restaurant VALUES(98, 'Maria Bonita', '', 1, '',0);
INSERT INTO restaurant VALUES(99, 'Mee-Ho Tapas Mexicaines', '', 1, '',0);
INSERT INTO restaurant VALUES(100, 'Melchorita', '', 1, '',0);
INSERT INTO restaurant VALUES(101, 'Merengue', '', 1, '',0);
INSERT INTO restaurant VALUES(102, 'Mesa 14', '', 1, '',0);
INSERT INTO restaurant VALUES(103, 'Mex-Hola', '', 1, '',0);
INSERT INTO restaurant VALUES(104, 'Mexico Restaurant', 'http://www.mexicorestaurante.ca', 1, '',0);
INSERT INTO restaurant VALUES(105, 'Mezkal', '', 1, '',0);
INSERT INTO restaurant VALUES(106, 'Mi Ranchito', '', 1, '',0);
INSERT INTO restaurant VALUES(107, 'Mochica', '', 1, '',0);
INSERT INTO restaurant VALUES(108, 'Mr. Azteca', '', 1, '',0);
INSERT INTO restaurant VALUES(109, 'Mucho Burrito', '', 1, '',0);
INSERT INTO restaurant VALUES(110, 'Nacho Libre', '', 1, '',0);
INSERT INTO restaurant VALUES(111, 'Pat'' e Palo', 'http://www.restaurantpatepalo.com', 1, '',7);
INSERT INTO restaurant VALUES(112, 'Piqueo''s Del Sur', '', 1, '',0);
INSERT INTO restaurant VALUES(113, 'Pizzelli Coq', '', 1, '',0);
INSERT INTO restaurant VALUES(114, 'Pucapuca (Puca Puca)', '', 1, '',0);
INSERT INTO restaurant VALUES(115, 'Que Rico', '', 1, '',0);
INSERT INTO restaurant VALUES(116, 'Restaurant Emiliano''s', '', 1, '',0);
INSERT INTO restaurant VALUES(117, 'Rodizio Brasil', 'http://www.rodiziobrasil.ca', 1, '',0);
INSERT INTO restaurant VALUES(118, 'Rosarito', '', 1, '',0);
INSERT INTO restaurant VALUES(119, 'Sabor Latino', 'http://saborlatino.ca', 1, '',0);
INSERT INTO restaurant VALUES(120, 'SeaSalt & Ceviche Br', '', 1, '',0);
INSERT INTO restaurant VALUES(121, 'SolyMar', 'http://www.restaurantsolymar.com', 1, '',0);
INSERT INTO restaurant VALUES(122, 'Super Taco', '', 1, '',0);
INSERT INTO restaurant VALUES(123, 'T & T tacos and tortas', '', 1, '',0);
INSERT INTO restaurant VALUES(124, 'Ta Chido (Tachido)', '', 1, '',0);
INSERT INTO restaurant VALUES(125, 'Taco Tijuana', '', 1, '',0);
INSERT INTO restaurant VALUES(126, 'Taco Trunp', '', 1, '',0);
INSERT INTO restaurant VALUES(127, 'Tacos Frida', 'http://www.rtacosfridamtl.ca', 1, '',0);
INSERT INTO restaurant VALUES(128, 'Tacos Tin Tan', '', 1, '',0);
INSERT INTO restaurant VALUES(129, 'Tacos Victor', '', 1, '',0);
INSERT INTO restaurant VALUES(130, 'Taqueria Arturo', 'http://www.taqueriaarturo.com', 1, '',0);
INSERT INTO restaurant VALUES(131, 'Taqueria La Matraca', '', 1, '',0);
INSERT INTO restaurant VALUES(132, 'Taqueria Mex', '', 1, '',0);
INSERT INTO restaurant VALUES(133, 'Tejano BBQ Burrito', '', 1, '',0);
INSERT INTO restaurant VALUES(134, 'Teque Teque', '', 1, '',0);
INSERT INTO restaurant VALUES(135, 'Tiradito', 'http://tiraditomtl.com/', 1, '',0);
INSERT INTO restaurant VALUES(136, 'Torteria Lupita', '', 1, '',0);
INSERT INTO restaurant VALUES(137, 'Villa Wellington', '', 1, '',0);
INSERT INTO restaurant VALUES(138, 'Z Tapas Lounge', 'http://ztapaslounge.com', 1, '',9);

INSERT INTO country VALUES (1,'Venezuela');
INSERT INTO country VALUES (2,'Colombia');
INSERT INTO country VALUES (3,'Argentina');
INSERT INTO country VALUES (4,'Mexico');
 
INSERT INTO location_desc VALUES(1, 'Ahunstic-Cartierville');
INSERT INTO location_desc VALUES(2, 'Anjou-St-Leonard');
INSERT INTO location_desc VALUES(3, 'Boisbriand');
INSERT INTO location_desc VALUES(4, 'Brossard');
INSERT INTO location_desc VALUES(5, 'Chomedey');
INSERT INTO location_desc VALUES(6, 'Cote-des-Neiges');
INSERT INTO location_desc VALUES(7, 'Crescent Street, Downtown');
INSERT INTO location_desc VALUES(8, 'Dollard-des-Ormeaux');
INSERT INTO location_desc VALUES(9, 'Downtown');
INSERT INTO location_desc VALUES(10, 'Hochelaga-Maisonneuve');
INSERT INTO location_desc VALUES(11, 'Lachine');
INSERT INTO location_desc VALUES(12, 'Latin Quarter');
INSERT INTO location_desc VALUES(13, 'Le Plateau-Mont-Royal');
INSERT INTO location_desc VALUES(14, 'Little Burgundy');
INSERT INTO location_desc VALUES(15, 'Little-Italy');
INSERT INTO location_desc VALUES(16, 'Mile-End');
INSERT INTO location_desc VALUES(17, 'Monkland Village');
INSERT INTO location_desc VALUES(18, 'Montreal-East');
INSERT INTO location_desc VALUES(19, 'Notre-Dame-de-Grace (NDG)');
INSERT INTO location_desc VALUES(20, 'Old-Montreal/Old Port');
INSERT INTO location_desc VALUES(21, 'Parc-Extension');
INSERT INTO location_desc VALUES(22, 'Pointe St-Charles');
INSERT INTO location_desc VALUES(23, 'Quartier des Spectacles');
INSERT INTO location_desc VALUES(24, 'Rosemont-Petite-Patrie');
INSERT INTO location_desc VALUES(25, 'Saint-Henri');
INSERT INTO location_desc VALUES(26, 'The Village/Centre-Sud');
INSERT INTO location_desc VALUES(27, 'Vaudreuil-Dorion');
INSERT INTO location_desc VALUES(28, 'Verdun');
INSERT INTO location_desc VALUES(29, 'Lasalle');
INSERT INTO location_desc VALUES(30, 'Vieux-Rosemont');
INSERT INTO location_desc VALUES(31, 'Villa Saint-Laurent');
INSERT INTO location_desc VALUES(32, 'Villerary-Saint Michel');
INSERT INTO location_desc VALUES(33, 'Westmount');

INSERT INTO restaurant_info VALUES(1, 1, 1, 3, '2965,Promenade St-Antoine. Boisbriand, QC, J7H 0B4','(450) 818-3393',1,null,null);
INSERT INTO restaurant_info VALUES(1, 1, 2, 4, '5605, Boul. Taschereau, Brossard, Greenfield Park, QC, J4Z 1A3', '(450) 926-0202', 1,null,null);
INSERT INTO restaurant_info VALUES(1, 1, 3, 5, '2033, Boul. St-Martin O. Laval, QC, H7T 2Y7', '(450) 682-6777', 1,null,null);
INSERT INTO restaurant_info VALUES(1, 1, 4, 9, '1657 Sainte-Catherine O. Montreal, QC, H3H 1L9', '(514) 939-3329', 1,null,null);
INSERT INTO restaurant_info VALUES(1, 1, 5, 12, '1621 Rue St-Denis Montreal, QC, H2X 3K3', '(514) 987-6868', 1,null,null);
INSERT INTO restaurant_info VALUES(1, 1, 6, 20, '301 Rue St-Antoine O. Montreal, QC, H2Z 2A7', '(514) 789-6006', 1,null,null);
INSERT INTO restaurant_info VALUES(2, 1, 1, 16, '5121 Boul. St-Laurent. Montreal, QC, H2T 2L9', '(514) 963-7064', 1,null,null);
INSERT INTO restaurant_info VALUES(3, 1, 1, 13, '1602 Ave.Laurier E. Montreal, QC, H2J 1H9  ', '(514) 564-8858', 1,null,null);
INSERT INTO restaurant_info VALUES(4, 1, 1, 17, '5974 Ave. de Monkland. Montreal, QC, H4A 1G8', '(514) 510-1225', 1,null,null);
INSERT INTO restaurant_info VALUES(5, 1, 1, 9, '575 Boul. de Maisonneuve W. Montreal, QC H3H 1L8', '(514) 334-5050', 1,null,null);
INSERT INTO restaurant_info VALUES(6, 1, 1, 19, '6000 Ave. Monkland. Montreal, QC, H4A 1G8', '(514) 357-6256', 1,null,null);
INSERT INTO restaurant_info VALUES(7, 1, 1, 13, '4050 Rue De Bullion. Montreal, QC, H2W 2E5', '(514) 508-7267', 1,null,null);
INSERT INTO restaurant_info VALUES(8, 1, 1, 13, '322 Ave. Mont-Royal E. Montreal, QC, H2T 1P7', '(514) 844-9999', 1,null,null);
INSERT INTO restaurant_info VALUES(9, 1, 1, 28, '4578 Rue Wellington. Montreal, QC H4G 1W7', '(514) 769-5885', 1,null,null);
INSERT INTO restaurant_info VALUES(10, 1, 1, 13, '1134 Ave. du Mont-Royal E. Montreal, QC, H2J 1X8', '(514)-525-7741', 1,null,null);
INSERT INTO restaurant_info VALUES(11, 1, 1, 16, '5210 Rue Saint-Urbain. Montreal, QC, H2T 2W8', '(514) 270-7369', 1,null,null);
INSERT INTO restaurant_info VALUES(12, 1, 1, 32, '1180 Rue Belanger E. Montreal, QC, H2S 1H4', '(514) 419-7826', 1,null,null);
INSERT INTO restaurant_info VALUES(13, 1, 1, 24, '1392 Rue Beaubien E. Montreal, QC, H2G 1K9', '(438) 381-4010', 1,null,null);
INSERT INTO restaurant_info VALUES(14, 1, 1, 13, '3677 Boul. Saint-Laurent. Montreal, QC, H2X 2V5', '(514) 227-4041', 1,null,null);
INSERT INTO restaurant_info VALUES(15, 1, 1, 15, '6918 Boul. St. Laurent. Montreal, Qc, H2S 3C7', '(514) 508-7172', 1,null,null);
INSERT INTO restaurant_info VALUES(16, 1, 1, 9, '2110 Rue Crescent. Montreal, QC, H3G 2B8', '(514) 379-4020', 1,null,null);
INSERT INTO restaurant_info VALUES(17, 1, 1, 2, '7999 Boul. Les Galeries d''Anjou.Montreal, QC, H1M 1W9', '(514) 983-5668', 1,null,null);
INSERT INTO restaurant_info VALUES(17, 1, 2, 4, '7405 Boul. Grande-Allee, Brossard, QC, J4Z 0E3', '(450) 812-9900', 1,null,null);
INSERT INTO restaurant_info VALUES(17, 1, 3, 5, '3003 Boul. Le Carrefour. Laval, QC, H7T 1C7 ', '(579) 640-3006', 1,null,null);
INSERT INTO restaurant_info VALUES(17, 1, 4, 10, '3905 Rue Ontario E. Montreal, QC, H1W 1S7', '(438) 386-6696', 1,null,null);
INSERT INTO restaurant_info VALUES(18, 1, 1, 13, '3686B Boul. St. Laurent. Montreal, QC, H2X 2V4', '(514) 289-2223', 1,null,null);
INSERT INTO restaurant_info VALUES(19, 1, 1, 32, '1453 Rue Belanger. Montreal, QC, H2G 1A5', '(514) 725-7208', 1,null,null);
INSERT INTO restaurant_info VALUES(20, 1, 1, 12, '153 Rue St-Catherine E. Montreal, QC H2X 1L1', '(514) 500-6259', 1,null,null);
INSERT INTO restaurant_info VALUES(21, 1, 1, 13, '1 Ave. du Mont-Royal O. Montreal, QC, H2T 2R9', '(514) 439-7720', 1,null,null);
INSERT INTO restaurant_info VALUES(22, 1, 1, 13, '4461 Rue St-Denis. Montreal, QC, H2J 2L2', '(514) 849-0349', 1,null,null);
INSERT INTO restaurant_info VALUES(23, 1, 1, 22, '1880 Rue Du Centre. Montreal, QC, H3K 1H9', '(514) 903-3511', 1,null,null);
INSERT INTO restaurant_info VALUES(24, 1, 1, 25, '4412 Rue Notre-Dame O. Montreal, QC, H4C 1S1', '(514) 553-2040', 1,null,null);
INSERT INTO restaurant_info VALUES(25, 1, 1, 9, '156 Rue Prince Arthur E. Montreal, QC, H2X 1B7', '(514) 508-8989', 1,null,null);
INSERT INTO restaurant_info VALUES(26, 1, 1, 31, '473 Boul. Cote-Vertu. Montreal, QC, H4L 1X7 ', '(514) 321-2968', 1,null,null);
INSERT INTO restaurant_info VALUES(27, 1, 1, 15, '67 Rue Beaubien E. Montreal, QC, H2S 1R1', '(514) 903-8585', 1,null,null);
INSERT INTO restaurant_info VALUES(28, 1, 1, 24, '2235 Rue Beaubien E. Montreal, QC, H2G 1M8', '(514) 701-0123', 1,null,null);
INSERT INTO restaurant_info VALUES(29, 1, 1, 13, '114 Ave. Laurier O. Montreal, QC, H2T 2N7', '(514) 227-8712', 1,null,null);
INSERT INTO restaurant_info VALUES(30, 1, 1, 9, '1420 Rue Peel. Montreal, QC, H3A 1S8', '(514) 288-3090', 1,null,null);
INSERT INTO restaurant_info VALUES(31, 1, 1, 24, '3217 Rue Beaubien E. Montreal, QC, H1Y 1H6', '(438) 380-6275', 1,null,null);
INSERT INTO restaurant_info VALUES(32, 1, 1, 32, '7467 Rue St-Hubert. Montreal, QC, H2R 2N5', '(514) 439-3313', 1,null,null);
INSERT INTO restaurant_info VALUES(33, 1, 1, 6, '5647 Chemin de la Cote-des-Neiges.Montreal, QC, H3T 1Y8', '(514) 341-6668', 1,null,null);
INSERT INTO restaurant_info VALUES(34, 1, 1, 24, '1121 Rue Belanger. Montreal, QC H2S 1H6', '(514) 508-5990', 1,null,null);
INSERT INTO restaurant_info VALUES(35, 1, 1, 15, '6580 Boul. St-Laurent. Montreal, QC, H2S 3C6', '(514) 277-9390', 1,null,null);
INSERT INTO restaurant_info VALUES(36, 2, 1, 26, '1481 Rue Amherst. Montreal, QC, H2L 3L2 ', '(514) 504-9015', 1,null,null);
INSERT INTO restaurant_info VALUES(37, 2, 1, 32, '7497 Rue St-Hubert. Montreal H2R 2N5', '(514) 271-6006', 1,null,null);
INSERT INTO restaurant_info VALUES(38, 2, 1, 15, '7102 Boul. St-Laurent. Montreal H2S 3E2', '(514) 379-4464', 1,null,null);
INSERT INTO restaurant_info VALUES(39, 2, 1, 9, '1855, Rue Ste-Catherine O. Montreal, QC, H3H 1M2 ', '(438) 382-1999', 1,null,null);
INSERT INTO restaurant_info VALUES(40, 2, 1, 31, '3641 Rue Fleury. Montreal, QC, H1H 2S5', '(514) 323-1110', 1,null,null);
INSERT INTO restaurant_info VALUES(41, 2, 1, 32, '7216 Rue Saint-Hubert. Montreal, QC, H2R 2N1', '(514) 276-3243', 1,null,null);
INSERT INTO restaurant_info VALUES(42, 2, 1, 16, '77 Rue Bernard O. Montreal, QC, H2T 2J9', '(514) 995-8056', 1,null,null);
INSERT INTO restaurant_info VALUES(43, 2, 1, 24, '51 Rue St. Zotique E. Montreal, QC, H2S 1K7', '(514) 278-4579', 1,null,null);
INSERT INTO restaurant_info VALUES(44, 2, 1, 24, '520 Rue Beaubien E. Montreal, QC, H2S 1S5', '(514) 272-5585', 1,null,null);
INSERT INTO restaurant_info VALUES(45, 2, 1, 14, '2518 Rue Notre-Dame O. Montreal, QC, H3J 2H4', '(438) 387-6969', 1,null,null);
INSERT INTO restaurant_info VALUES(46, 2, 1, 32, '7245 Rue St-Hubert. Montreal, QC, H2R 2N2', '(514) 272-3096', 1,null,null);
INSERT INTO restaurant_info VALUES(46, 2, 2, 32, '7183  Rue St-Hubert. Montreal, QC, H2R 2N2', '(514) 948-4827', 1,null,null);
INSERT INTO restaurant_info VALUES(47, 2, 1, 11, '1678 Boul. St-Joseph. Montreal, QC, H8S 2N1', '(514) 634-0442', 1,null,null);
INSERT INTO restaurant_info VALUES(48, 2, 1, 15, '232 Rue Jean Talon (Marche Jean Talon) Montreal, QC, H2R 1S7', '(514) 274-3336', 1,null,null);
INSERT INTO restaurant_info VALUES(49, 2, 1, 28, '5013 Rue Wellington. Montreal, QC, H4G 1Y1', '(514) 362-8888', 1,null,null);
INSERT INTO restaurant_info VALUES(50, 2, 1, 1, '70 Rue Chabanel O. Montreal, QC, H2N 1E7', '(514) 303-0240', 1,null,null);
INSERT INTO restaurant_info VALUES(51, 2, 1, 13, '1276 Ave. Laurier E. Montreal, QC, H2J 1H1', '(514) 598-0344', 1,null,null);
INSERT INTO restaurant_info VALUES(52, 2, 1, 32, '9086 Boul. St-Michel. Montreal, QC, H1Z 3G5', '(514) 387-1387', 1,null,null);
INSERT INTO restaurant_info VALUES(53, 2, 1, 9, '1224 Rue Drummond. Montreal, QC, H3G 1V7', '(514) 375-5945', 1,null,null);
INSERT INTO restaurant_info VALUES(53, 2, 2, 9, '1206  Rue Union. Montreal, QC, H3B 3C4', '(514) 419-9755', 1,null,null);
INSERT INTO restaurant_info VALUES(54, 2, 1, 24, '6648 Boul. St-Laurent. Montreal, QC, H2S 3C4', '(514) 303-3111', 1,null,null);
INSERT INTO restaurant_info VALUES(55, 2, 1, 25, '630 Rue de Courcelles. Montreal, QC, H4C 3C5', '(514) 290-5125', 1,null,null);
INSERT INTO restaurant_info VALUES(56, 2, 1, 29, '1484 Boul. Shevshenko. Montreal, QC, H8N 1M1', '(514) 266-4919', 1,null,null);
INSERT INTO restaurant_info VALUES(57, 2, 1, 13, '51 Rue Roy. Montreal, QC, H2W 2S3', '(514) 439-6691', 1,null,null);
INSERT INTO restaurant_info VALUES(58, 2, 1, 1, '1486 Rue de l''Eglise. Montreal, QC, H4L 2H6', '(514) 747-0800', 1,null,null);
INSERT INTO restaurant_info VALUES(59, 2, 1, 32, '50 Rue Jarry E. Montreal, QC H2P 1T1', '(514) 381-2992', 1,null,null);
INSERT INTO restaurant_info VALUES(60, 2, 1, 20, '52 Rue St-Jacques Montreal, QC, H2Y 1L2', '(514) 508-4075', 1,null,null);
INSERT INTO restaurant_info VALUES(61, 2, 1, 24, '67 Rue Beaubien E. Montreal, QC, H2S 1R1', '(514) 507-5517', 1,null,null);
INSERT INTO restaurant_info VALUES(62, 2, 1,  1, '44 Rue Beaubien O. Montreal, QC, H2S 1V3', '(514) 490-0044', 1,null,null);
INSERT INTO restaurant_info VALUES(63, 2, 1, 32, '7805 Boul. St-Laurent. Montreal, QC, H2R 1X1', '(514) 277-0021', 1,null,null);
INSERT INTO restaurant_info VALUES(64, 2, 1, 23, '1096 Boul. St-Laurent. Montreal, QC, H2Z 1J5', '(514) 873-5255', 1,null,null);
INSERT INTO restaurant_info VALUES(65, 2, 1, 24, '350 Rue Saint-Zotique E. Montreal, QC, H2S 1L7', '(514) 273-8884', 1,null,null);
INSERT INTO restaurant_info VALUES(66, 2, 1, 24, '500 Rue Belanger. Montreal, QC, H2S 1G4', '(514) 660-3879', 1,null,null);
INSERT INTO restaurant_info VALUES(67, 2, 1, 13, '100 Rue Marie-Anne O. Montreal, QC, H2W 1B9', '(514) 982-9212', 1,null,null);
INSERT INTO restaurant_info VALUES(68, 2, 1, 20, '36 Rue Notre-Dame O. Montreal, QC, H2Y 1S6 ', '(438) 381-4488', 1,null,null);
INSERT INTO restaurant_info VALUES(69, 2, 1, 26, '2345 Rue Ontario E. Montréal, QC, H2K 1W2', '(514) 523-3262', 1,null,null);
INSERT INTO restaurant_info VALUES(70, 2, 1, 29, '1659 Ave. Dollard. Montreal, QC, H8N 1T7', '(514) 419-2266', 1,null,null);
INSERT INTO restaurant_info VALUES(71, 3, 1, 9, '1216 Rue Union. Montreal, QC, H3B 3C4', '(514) 375-5355', 1,null,null);
INSERT INTO restaurant_info VALUES(72, 3, 1, 13, '862 Rue Marie-Anne E. Montreal, QC, H2J 2A9', '(514) 525-1798', 1,null,null);
INSERT INTO restaurant_info VALUES(73, 3, 1, 16, '226 Ave. Fairmount O. Montreal, QC, H2T 2MT', '(438) 381-5034', 1,null,null);
INSERT INTO restaurant_info VALUES(74, 3, 1, 24, '1190 Rue Belanger E. Montreal, QC, H2S 1H4', '(514) 277-9908', 1,null,null);
INSERT INTO restaurant_info VALUES(75, 3, 1, 32, '7476 Rue Saint-Hubert. Montreal, QC, H2R 2N3', '(514) 270-3785', 1,null,null);
INSERT INTO restaurant_info VALUES(76, 3, 1, 9, '1458 Rue Crescent. Montreal, QC, H3G 2B6', '(514) 439-8383', 1,null,null);
INSERT INTO restaurant_info VALUES(77, 3, 1, 33, '374 Ave. Victoria. Montreal, QC, H3Z 2N1', '(514) 303-4123', 1,null,null);
INSERT INTO restaurant_info VALUES(78, 3, 1, 14, '2705 Rue Notre-Dame O. Montreal, QC, H3J 1N9', '(514) 316-4585', 1,null,null);
INSERT INTO restaurant_info VALUES(79, 3, 1, 32, '2566 Rue Jean-Talon E. Montreal, QC, H2A 1T9', '(514) 722-3801', 1,null,null);
INSERT INTO restaurant_info VALUES(80, 3, 1, 13, '19 Ave. du Mont-Royal E. Montreal, QC, H2T 1N4', '(514) 281-0871', 1,null,null);
INSERT INTO restaurant_info VALUES(81, 3, 1, 4, '7845 Boul. Taschereau, Brossard, QC, J4Y 1A4', '(450) 904-5172', 1,null,null);
INSERT INTO restaurant_info VALUES(81, 3, 2, 5, '579 Boul. St-Martin O. Laval, QC, H7M 1Y9', '(450) 967-7770', 1,null,null);
INSERT INTO restaurant_info VALUES(81, 3, 3, 9, '1476 Rue Crescent. Montreal, QC, H3G2B6', '(514) 985-0777', 1,null,null);
INSERT INTO restaurant_info VALUES(81, 3, 4, 8, '5021 Boul. Des Sources, West Island (Montreal) QC, H8Y 3E3', '(514) 542-4224', 1,null,null);
INSERT INTO restaurant_info VALUES(82, 3, 1, 20, '153 Rue St-Amable. Montreal, QC, H2Y 1E7', '(514) 379-2010', 1,null,null);
INSERT INTO restaurant_info VALUES(83, 3, 1, 32, '2474 Rue Jean Talon E. Montreal, QC, H2E 1W2', '(514) 374-7448', 1,null,null);
INSERT INTO restaurant_info VALUES(84, 3, 1, 25, '4401 Rue Notre-Dame O. Montreal, QC, H4C 1S2', '(514) 303-4401', 1,null,null);
INSERT INTO restaurant_info VALUES(84, 3, 2, 31, '972 Boul. Decaire. Montreal, QC, H4L 3M5 ', '(514) 331-8753', 1,null,null);
INSERT INTO restaurant_info VALUES(85, 3, 1, 10, '2212 Rue St-Germain.Montreal, QC, H1W 2T7', '(514) 598-1234', 1,null,null);
INSERT INTO restaurant_info VALUES(86, 3, 1, 12, '2005 Rue St-Denis. Montreal, QC, H2X 3K8', '(514) 659-7218', 1,null,null);
INSERT INTO restaurant_info VALUES(87, 3, 1, 32, '2379 Rue Belanger.Montreal, QC, H2G 1E4', '(514) 729-0222', 1,null,null);
INSERT INTO restaurant_info VALUES(88, 3, 1, 26, '1371 Rue Sainte-Catherine E. Montreal, QC, H2L 2H7', '(514) 379-1371', 1,null,null);
INSERT INTO restaurant_info VALUES(89, 3, 1, 13, '4115 Rue Saint Denis. Montreal, QC, H2W 2M7', '(514) 419-8789', 1,null,null);
INSERT INTO restaurant_info VALUES(90, 3, 1, 24, '531 Rue Belanger. Montreal, QC, H2S 1G5', '(514) 277-3678', 1,null,null);
INSERT INTO restaurant_info VALUES(91, 3, 1, 13, '921 Ave. du Mont Royal E. Montreal, QC, H2J 1X3', '(514) 303-1050', 1,null,null);
INSERT INTO restaurant_info VALUES(92, 3, 1, 9, '2053 Rue Peel. Montreal, QC, H3A 1T6', '(514) 379-1055', 1,null,null);
INSERT INTO restaurant_info VALUES(92, 3, 2, 9, '1530 Boul. de Maisonneuve O. Montreal, QC, H3G 1N1 ', '(514) 508-3478', 1,null,null);
INSERT INTO restaurant_info VALUES(92, 3, 3, 12, '1773 Rue St-Denis. Montreal, QC, H2x 3K4', '(438) 380-8090', 1,null,null);
INSERT INTO restaurant_info VALUES(93, 3, 1, 16, '5171 Ave du Parc. Montreal, QC, H2V 4G3', '(514) 508-5038', 1,null,null);
INSERT INTO restaurant_info VALUES(94, 3, 1, 1, '124  Rue Fleury Ouest. Montreal, QC, H3L 1T4', '(514) 439-1966', 1,null,null);
INSERT INTO restaurant_info VALUES(94, 3, 2, 30, '2931 Rue Masson Montréal, QC, H1Y 1X5', '(514) 315-7932', 1,null,null);
INSERT INTO restaurant_info VALUES(95, 3, 1, 16, '5439 Boul. St-Laurent. Montreal, QC, H2T 1S5', '(514) 507-7740', 1,null,null);
INSERT INTO restaurant_info VALUES(96, 3, 1, 13, '2 Rue Sherbrooke E. Montreal, QC, H2X 1C2', '(514) 844-8220', 1,null,null);
INSERT INTO restaurant_info VALUES(97, 3, 1, 13, '3605 Rue St-Denis. Montreal, QC, H2X 3L6', '(514) 847-1050', 1,null,null);
INSERT INTO restaurant_info VALUES(98, 3, 1, 13, '5269 Ave. Casgrain. Montreal, QC, H2T 1X1', '(514) 807-4377', 1,null,null);
INSERT INTO restaurant_info VALUES(99, 3, 1, 24, '6670 Rue St-Hubert. Montreal, QC, H2S 2M3', '(438) 382-6670', 1,null,null);
INSERT INTO restaurant_info VALUES(100, 3, 1, 32, '7901 Rue St-Dominique. Montreal, QC, H2R 1X8', '(514) 382-2129', 1,null,null);
INSERT INTO restaurant_info VALUES(101, 3, 1, 32, '9289 Boul. St-Michel. Montreal, QC, H1Z 3G7', '(514) 507-9979', 1,null,null);
INSERT INTO restaurant_info VALUES(102, 3, 1, 9, '1425 Rue Bishop. Montreal, QC, H3G 2E4', '(514) 284-0344', 1,null,null);
INSERT INTO restaurant_info VALUES(103, 3, 1, 12, '1709 Rue Saint-Denis. Montreal, QC, H2X 3K4', '(514) 303-4651', 1,null,null);
INSERT INTO restaurant_info VALUES(104, 3, 1, 32, '2524 Rue Jean-Talon E. Montreal, QC, H2A 1T7', '(514) 379-1450', 1,null,null);
INSERT INTO restaurant_info VALUES(105, 3, 1, 16, '5834 Ave. du Parc. Montreal, QC, H2V 4H3', '(514) 277-2377', 1,null,null);
INSERT INTO restaurant_info VALUES(106, 3, 1, 18, '4565 Rue Belanger. Montreal, QC, H1T 1A3', '(514) 727-7778', 1,null,null);
INSERT INTO restaurant_info VALUES(106, 3, 2, 31, '1486 Rue de l''Eglise. Montreal, QC, H4L 2H6', '(514) 747-7765', 1,null,null);
INSERT INTO restaurant_info VALUES(107, 3, 1, 13, '3863 Rue St-Denis. Montreal, QC, H2W 2M4', '(514) 284-4448', 1,null,null);
INSERT INTO restaurant_info VALUES(108, 3, 1, 32, '7349 Rue St-Hubert. Montreal, QC, H2R 2N4', '(438) 387-3876', 1,null,null);
INSERT INTO restaurant_info VALUES(109, 3, 1, 1, '1029 Rue du Marche Central. Montreal, QC, H4N 1J8', '(514) 381-9393', 1,null,null);
INSERT INTO restaurant_info VALUES(109, 3, 2, 4, '8015 Boul. du Quartier, J4Y 0N6', '(450) 443-9888', 1,null,null);
INSERT INTO restaurant_info VALUES(109, 3, 3, 27, '22800 chemin Dumberry, Suite #10, J7V 0M8', '(450) 455-5300', 1,null,null);
INSERT INTO restaurant_info VALUES(109, 3, 4, 31, '3830 Boul. Cote-Vertu. Montreal, QC, H4R 1P8', '(514) 332-0880', 1,null,null);
INSERT INTO restaurant_info VALUES(110, 3, 1, 24, '913 Rue Beaubien E. Montreal, QC, H2S 1T2', '(514) 273-6222', 1,null,null);
INSERT INTO restaurant_info VALUES(111, 3, 1, 24, '2534 Rue Beaubien E. Montreal, QC ,H1Y 1G2', '(514) 315-5521', 1,null,null);
INSERT INTO restaurant_info VALUES(112, 3, 1, 21, '77 Rue Jean Talon E. Montreal, QC, H2R 1S6', '(514) 270-2424', 1,null,null);
INSERT INTO restaurant_info VALUES(113, 3, 1, 32, '7616 Rue St-Hubert. Montreal, QC, H2R 2N6', '(514) 495-0111', 1,null,null);
INSERT INTO restaurant_info VALUES(114, 4, 1, 13, '5400 B. St-Laurent. Montreal, QC, H2T 1S1', '(514) 272-8029', 1,null,null);
INSERT INTO restaurant_info VALUES(115, 4, 1, 26, '1659 Rue Ontario. Montreal, QC, H2L 1S8', '(514) 922-5826', 1,null,null);
INSERT INTO restaurant_info VALUES(116, 4, 1, 20, '260 Rue Notre-Dame O. Montreal, QC, H2Y 1T6', '(514) 316-8017', 1,null,null);
INSERT INTO restaurant_info VALUES(117, 4, 1, 20, '160 Rue Notre-Dame E. Montreal, QC, H2Y 1C2', '(514) 508-3883', 1,null,null);
INSERT INTO restaurant_info VALUES(118, 4, 1, 16, 'Mile-end. Montreal,QC, H2T 2K9', '(514) 497-6982', 1,null,null);
INSERT INTO restaurant_info VALUES(119, 4, 1, 13, '4387 Boul. Saint Laurent. Montreal, QC H2W 1Z8', '(514) 848-1078', 1,null,null);
INSERT INTO restaurant_info VALUES(119, 4, 2, 24, '436 Rue Belanger. Montreal, QC, H2S 1G2', '(514) 277-4130', 1,null,null);
INSERT INTO restaurant_info VALUES(120, 4, 1, 20, '351 Place Royale. Montreal, QC, H2Y 2V2', '(514) 937-6555', 1,null,null);
INSERT INTO restaurant_info VALUES(121, 4, 1, 32, '7610 Rue St-Hubert. Montreal, QC, H2R 2N6', '(514) 273-4446', 1,null,null);
INSERT INTO restaurant_info VALUES(122, 4, 1, 24, '500-A Rue Belanger E.Montreal, QC, H2S 1G4 ', '(514) 357-9335', 1,null,null);
INSERT INTO restaurant_info VALUES(123, 4, 1, 13, '51 Rue Rachel O. Montreal, QC, H2W 1G2', '(514) 518-5331', 1,null,null);
INSERT INTO restaurant_info VALUES(124, 4, 1, 16, '5611 Ave. du Parc. Montreal, QC, H2V 4H2', '(514) 439-0935', 1,null,null);
INSERT INTO restaurant_info VALUES(125, 4, 1, 17, '5893 Rue Sherbrooke O. Montreal, QC, H4A 1X6', '(514) 419-5450', 1,null,null);
INSERT INTO restaurant_info VALUES(126, 4, 1, 15, '388 Rue Saint-Zotique E. Montreal, QC, H2S 1L5', '(514) 661-5060', 1,null,null);
INSERT INTO restaurant_info VALUES(127, 4, 1, 25, '4350 Rue Notre-Dame O. Montreal, QC H4C 2W6', '(514) 316-3255', 1,null,null);
INSERT INTO restaurant_info VALUES(128, 4, 1, 13, '3828 Rue St-Denis. Montreal, QC, H2W 2M2', '(514) 992-6664', 1,null,null);
INSERT INTO restaurant_info VALUES(129, 4, 1, 16, '114 Ave. Laurier O. Montreal, QC, H2T 2N7', '(514) 277-9500', 1,null,null);
INSERT INTO restaurant_info VALUES(129, 4, 2, 25, '4376 Rue. Notre-Dame O. Montreal, QC, H4C 1R8', '(514) 932-6837', 1,null,null);
INSERT INTO restaurant_info VALUES(130, 4, 1, 13, '64 Rue Prince Arthur E. Montreal, QC, H2X 1B3', '(514) 400-7050', 1,null,null);
INSERT INTO restaurant_info VALUES(131, 4, 1, 13, '4607 Rue St-Denis. Montreal, QC, H2J 2L4', '(514) 285-0777', 1,null,null);
INSERT INTO restaurant_info VALUES(132, 4, 1, 16, '4306 Boul. Saint Laurent. Montreal, QC H2W 1Z3', '(514) 982-9462', 1,null,null);
INSERT INTO restaurant_info VALUES(133, 4, 1, 20, '729 Rue William. Montreal, QC, H3C 1P1', '(514) 379-3131', 1,null,null);
INSERT INTO restaurant_info VALUES(133, 4, 2, 25, '511 Rue Courcelle. Montreal, QC, H4C 3C1', '(514) 303-3500', 1,null,null);
INSERT INTO restaurant_info VALUES(134, 4, 1, 24, '6583 Boul. St-Laurent. Montreal, QC, H2S 3C5', '(514) 303-4200', 1,null,null);
INSERT INTO restaurant_info VALUES(135, 4, 1, 9, '1076 Rue de Bleury. Montreal, QC, H2Z 1N2', '(514) 866-6776', 1,null,null);
INSERT INTO restaurant_info VALUES(136, 4, 1, 25, '4601 Rue Notre-Dame O. Montreal, QC,H4C 1S5 ', '(514) 989-8464', 1,null,null);
INSERT INTO restaurant_info VALUES(137, 4, 1, 28, '4701 Rue Wellimgton. Montreal, QC, H4G 1X2', '(514) 768-0102', 1,null,null);
INSERT INTO restaurant_info VALUES(138, 4, 1, 23, '1165 Boul. St-Laurent. Montreal, QC, H2X 2S6', '(514) 903-4040', 1,null,null);

INSERT INTO restaurant_days VALUES(1,1,'Viernes','16:00','22:00');
INSERT INTO restaurant_days VALUES(1,2,'Sabado','16:00','22:00');
INSERT INTO restaurant_days VALUES(1,3,'Domingo','16:00','22:00');
INSERT INTO restaurant_days VALUES(1,4,'Domingo','18:00','22:00');

INSERT INTO card_brand VALUES(1,'Visa',1);
INSERT INTO card_brand VALUES(2,'Master Card',1);
INSERT INTO card_brand VALUES(3,'American Express',0);

INSERT INTO entry_method VALUES (1,'EMV',1);
INSERT INTO entry_method VALUES (2,'Contactless',0);
INSERT INTO entry_method VALUES (3,'Cash',1);

INSERT INTO restaurant_pay_info VALUES (1,3,1,1);
INSERT INTO restaurant_pay_info VALUES (2,1,null,1);

INSERT INTO rating (id_restaurant,id_address,user_name,rating,comment_desc) VALUES (1,1,'tina22',5,'Fino'); 
INSERT INTO rating (id_restaurant,id_address,user_name,rating,comment_desc) VALUES (1,1,'gael17',4,'Bien');
INSERT INTO rating (id_restaurant,id_address,user_name,rating,comment_desc) VALUES (1,3,'tina22',3,'Malo');
INSERT INTO rating (id_restaurant,id_address,user_name,rating,comment_desc) VALUES (1,3,'gael17',3,'');
INSERT INTO rating (id_restaurant,id_address,user_name,rating,comment_desc) VALUES (2,1,'tina22',3,'');
INSERT INTO rating (id_restaurant,id_address,user_name,rating,comment_desc) VALUES (2,1,'gael17',2,'');
INSERT INTO rating (id_restaurant,id_address,user_name,rating,comment_desc) VALUES (3,2,'tina22',1,'');
INSERT INTO rating (id_restaurant,id_address,user_name,rating,comment_desc) VALUES (3,2,'gael17',5,'');

INSERT INTO food_types VALUES (1, 'Afghan');
INSERT INTO food_types VALUES (2, 'African');
INSERT INTO food_types VALUES (3, 'American');
INSERT INTO food_types VALUES (4, 'Argentinean');
INSERT INTO food_types VALUES (5, 'Asian');
INSERT INTO food_types VALUES (6, 'BBQ');
INSERT INTO food_types VALUES (7, 'Bisrtro');
INSERT INTO food_types VALUES (8, 'Bistro');
INSERT INTO food_types VALUES (9, 'Brazilian');
INSERT INTO food_types VALUES (10, 'Breakfast');
INSERT INTO food_types VALUES (11, 'Burgers');
INSERT INTO food_types VALUES (12, 'Café');
INSERT INTO food_types VALUES (13, 'Cajun');
INSERT INTO food_types VALUES (14, 'Californian');
INSERT INTO food_types VALUES (15, 'Canadian');
INSERT INTO food_types VALUES (16, 'Caribbean');
INSERT INTO food_types VALUES (17, 'Chilean');
INSERT INTO food_types VALUES (18, 'Colombian');
INSERT INTO food_types VALUES (19, 'Comfort Food');
INSERT INTO food_types VALUES (20, 'Contemporary/Modern');
INSERT INTO food_types VALUES (21, 'Desserts & Sweets');
INSERT INTO food_types VALUES (22, 'Dominican');
INSERT INTO food_types VALUES (23, 'Exotic');
INSERT INTO food_types VALUES (24, 'Fish &Chips');
INSERT INTO food_types VALUES (25, 'French');
INSERT INTO food_types VALUES (26, 'Fusion');
INSERT INTO food_types VALUES (27, 'Grill');
INSERT INTO food_types VALUES (28, 'Hawaiian');
INSERT INTO food_types VALUES (29, 'Healthy');
INSERT INTO food_types VALUES (30, 'International');
INSERT INTO food_types VALUES (31, 'Italian');
INSERT INTO food_types VALUES (32, 'Japanese');
INSERT INTO food_types VALUES (33, 'Juice Bar & Smoothies');
INSERT INTO food_types VALUES (34, 'Latin American');
INSERT INTO food_types VALUES (35, 'Latin American Tapas');
INSERT INTO food_types VALUES (36, 'Market');
INSERT INTO food_types VALUES (37, 'Mexican');
INSERT INTO food_types VALUES (38, 'Pasta');
INSERT INTO food_types VALUES (39, 'Pastries');
INSERT INTO food_types VALUES (40, 'Peruvian');
INSERT INTO food_types VALUES (41, 'Pizza');
INSERT INTO food_types VALUES (42, 'Poke Bowl');
INSERT INTO food_types VALUES (43, 'Portuguese');
INSERT INTO food_types VALUES (44, 'Poutine');
INSERT INTO food_types VALUES (45, 'Pub Food');
INSERT INTO food_types VALUES (46, 'Rotisserie');
INSERT INTO food_types VALUES (47, 'Russian');
INSERT INTO food_types VALUES (48, 'Salvadorian');
INSERT INTO food_types VALUES (49, 'Sandwiches');
INSERT INTO food_types VALUES (50, 'Seafood/Fish');
INSERT INTO food_types VALUES (51, 'Steakhouse');
INSERT INTO food_types VALUES (52, 'Tapas');
INSERT INTO food_types VALUES (53, 'Tartare');
INSERT INTO food_types VALUES (54, 'Urban');
INSERT INTO food_types VALUES (55, 'Vegan');
INSERT INTO food_types VALUES (56, 'Vegetarian');
INSERT INTO food_types VALUES (57, 'Venezuelan');
INSERT INTO food_types VALUES (58, 'Vietnamese');


INSERT INTO restaurant_food_type VALUES (1,1);
INSERT INTO restaurant_food_type VALUES (1,2);
INSERT INTO restaurant_food_type VALUES (2,3);

