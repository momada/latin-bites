/*drop table restaurant_food_type;
drop table food_types;
drop table rating;
drop table restaurant_pay_info;
drop table entry_method;
drop table card_brand;
drop table restaurant_days;
drop table restaurant_info;
drop table location_desc;
drop table restaurant_info;
drop table country;
drop table restaurant;*/
--CREATE DATABASE LatinVibes
--use LatinVibes

CREATE TABLE restaurant 
  ( 
     id_restaurant      INT, 
     restaurant_name    VARCHAR(50), 
     restaurant_website VARCHAR(50), 
     enabled            BIT, 
     logo               VARCHAR(50), 
	 trending           INT,
     PRIMARY KEY (id_restaurant) 
  ); 

CREATE TABLE country 
  ( 
     id_country   INT, 
     country_name VARCHAR(50), 
     PRIMARY KEY (id_country) 
  ); 

CREATE TABLE location_desc 
  ( 
     id_location   INT, 
     location_desc VARCHAR(50), 
     PRIMARY KEY (id_location) 
  ); 

CREATE TABLE restaurant_info 
  ( 
     id_restaurant INT, 
     id_country    INT, 
     id_address    INT, 
     id_location   INT, 
     address_desc  VARCHAR (80), 
     phone_number  VARCHAR (20), 
     yn_delivery   BIT, 
     longitude     DECIMAL(20, 10), 
     latitude      DECIMAL(20, 10), 
     PRIMARY KEY (id_restaurant, id_address), 
     FOREIGN KEY (id_restaurant) REFERENCES restaurant(id_restaurant), 
     FOREIGN KEY (id_location) REFERENCES location_desc(id_location), 
     FOREIGN KEY (id_country) REFERENCES country(id_country) 
  ); 

CREATE TABLE restaurant_days 
  ( 
     id_restaurant INT, 
     id_address    INT, 
     open_days     VARCHAR(7), 
     opening_time  TIME, 
     closing_time  TIME, 
     PRIMARY KEY (id_restaurant, id_address), 
     FOREIGN KEY (id_restaurant) REFERENCES restaurant(id_restaurant) 
  ); 

CREATE TABLE card_brand 
  ( 
     id_brand   INT, 
     brand_desc VARCHAR(30), 
     enabled    BIT, 
     PRIMARY KEY (id_brand) 
  ); 

CREATE TABLE entry_method 
  ( 
     id_entry   INT, 
     entry_desc VARCHAR(30), 
     enabled    BIT, 
     PRIMARY KEY (id_entry) 
  ); 

CREATE TABLE restaurant_pay_info 
  ( 
     id_restaurant INT, 
     id_address    INT, 
     id_brand      INT, 
     id_entry      INT, 
     FOREIGN KEY (id_restaurant) REFERENCES restaurant(id_restaurant), 
     FOREIGN KEY (id_brand) REFERENCES card_brand(id_brand), 
     FOREIGN KEY (id_entry) REFERENCES entry_method(id_entry) 
  ); 

CREATE TABLE rating 
  ( 
     id_restaurant INT, 
     id_address    INT, 
     user_name     VARCHAR(30), 
     rating        INT, 
     comment_desc  VARCHAR(300), 
     date_time     DATETIME DEFAULT GETDATE(), 
     PRIMARY KEY (id_restaurant, user_name), 
     FOREIGN KEY (id_restaurant) REFERENCES restaurant(id_restaurant) 
  ); 

CREATE TABLE food_types 
  ( 
     id_type   INT, 
     type_desc VARCHAR(30), 
     PRIMARY KEY(id_type) 
  ); 

CREATE TABLE restaurant_food_type 
  ( 
     id_restaurant INT, 
     id_type       INT
     FOREIGN KEY (id_restaurant) REFERENCES restaurant(id_restaurant), 
     FOREIGN KEY (id_type) REFERENCES food_types(id_type) 
  ); 