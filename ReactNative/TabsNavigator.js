import type {
    NavigationScreenProp,
    NavigationEventSubscription,
  } from 'react-navigation';
import React from 'react';
import {Icon, View, StyleSheet, Text, ScrollView, Button, TouchableOpacity} from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';
import Ionicons from 'react-native-vector-icons/Ionicons';

const NavScreen = ({ navigation, banner }) => (
    <Text style={styles.banner}>{banner}</Text>
);

const MenuButton  = (
    <View>
        <TouchableOpacity onPress={() => { this.props.navigate('DrawerOpen')} }>
            <Icon name="bars" style={{color: 'white', padding: 10, marginLeft:10, fontSize: 20}}/>
        </TouchableOpacity>
    </View>
);

const HomeScreen = ({ navigation, franchiseName }) => (
    <ScrollView>
        <NavScreen navigation={ navigation } banner="Home!" />
        <Button
            onPress={() => navigation.navigate('FranchiseScreenNav', { name: "Test"})}
            title = "Go to restaurant!"
        />
    </ScrollView>
);

const RestaurantScreen = ({ navigation }) => (
    <ScrollView>
        <NavScreen navigation={ navigation } banner="Restaurants!" />
    </ScrollView>
);

const EventScreen = ({ navigation }) => (
    <NavScreen navigation={ navigation } banner="Events!" />
);

const FavoriteScreen = ({ navigation }) => (
    <NavScreen navigation={ navigation } banner="Favorites!" />
);

const FranchiseScreen = ({ navigation}) => (
    <NavScreen navigation= { navigation } 
    banner = {`${navigation.state.params.name}`} />
)

const TabsNavigator = createBottomTabNavigator(
    {
        HomeTab: {
            screen: HomeScreen,
            path: '/',
            navigationOptions: {
                tabBarLabel: 'Home',
                tabBarIcon: ({ tintColor, focused }) => (
                    <Ionicons
                      name={'ios-home'}
                      size={26}
                      style={{ color: tintColor }}
                    />
                  ),
            },
        },
        RestaurantTab: {
            screen: RestaurantScreen,
            path: '/restaurants',
            navigationOptions: {
                title: 'Restaurant Title',
                tabBarLabel: 'Restaurants'
            },
        },
        EventTab: {
            screen: EventScreen,
            path: '/events',
            navigationOptions: {
                title: 'Events Title',
                tabBarLabel: 'Events'
            },
        },
        FavoritesTab: {
            screen: FavoriteScreen,
            path: '/favorites',
            navigationOptions: {
                title: 'Favorites title',
                tabBarLabel: 'Favorites'
            },
        },
    },
    {
        tabBarPosition: 'bottom',
        animationEnabled: false,
        swipeEnabled: false,
    }
);

TabsNavigator.navigationOptions = ({ navigation }) => {
    let { routeName } = navigation.state.routes[navigation.state.index];
    let title;
    let headerRight;
    let headerLeft = (
        <Button
        onPress={() => navigation.openDrawer()}
        title="="
        />
    );
 
    if( routeName === 'HomeTab' ) {
        title = 'Home tab text'
        headerRight = (
            <Button
                onPress={() => navigation.navigate('FranchiseScreenNav', { name: "Test"})}
                title="Filter"
            />
        )
    }
    else if ( routeName === 'RestaurantTab' ) {
        title = 'Restaurants'
    }
    else if ( routeName === 'EventTab' ) {
        title = 'Events'
    }
    else if ( routeName === 'FavoritesTab' ) {
        title = 'Favorites'
    }

    return { 
        title, headerRight, headerLeft
    };
};

const StacksOverTabs = createStackNavigator({
    Root: {
      screen: TabsNavigator,
    },
    HomeScreenNav: {
        screen: HomeScreen,
        navigationOptions: ({ navigation }) => ({
        title: 'Home Screen',
        }),
    },
    RestaurantScreenNav: {
        screen: RestaurantScreen,
        navigationOptions: ({ navigation }) => ({
            title: 'Restaurant Screen',
        }),
    },
    EventScreenNav: {
        screen: EventScreen,
        navigationOptions: ({ navigation }) => ({
          title: 'Event Screen',
        }),
    },
    FavoriteScreenNav: {
        screen: FavoriteScreen,
        navigationOptions: ({ navigation }) => ({
          title: 'Favorite Screen',
        }),
    },
    FranchiseScreenNav: {
        screen: FranchiseScreen,
        navigationOptions: ({ navigation }) => ({
            title: `${navigation.state.params.name}`,
        }),
    }
  });

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    banner: {
        margin: 14,
    }
});

export default StacksOverTabs;