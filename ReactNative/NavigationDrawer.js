import React from 'react';
import { createDrawerNavigator } from 'react-navigation';
import TabsNavigator from './TabsNavigator';
import AccountFlow from './AccountFlow'

const TabsInDrawer = createDrawerNavigator({
    Home: {
        screen: TabsNavigator,
        navigationOptions: {
            drawerLabel: 'Home',
        },
    },
    AccountOption: {
        screen: AccountFlow, 
        navigationOptions :  {
            drawerLabel: 'Account',
        },
    },
    LanguageOption: {
        screen: TabsNavigator, 
        navigationOptions : {
            drawerLabel: 'Language',
            },
    },
    LocationOption: {
        screen: TabsNavigator, 
        navigationOptions : {
            drawerLabel: 'Location',
            },
    },
});
  
  export default TabsInDrawer;