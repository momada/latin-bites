import { SwitchNavigator, StackNavigator } from 'react-navigation'

import Account from './Account'
import SignUp from './SignUp'
import Login from './Login'
import Loading from './Loading'
import NavigationDrawer from './NavigationDrawer'

const AuthStack = StackNavigator ({
    SignUp,
    Login,
});

const AccountStack = StackNavigator ({
    Account
});

const AccountFlow = SwitchNavigator (
    {
        LoadingScreen: Loading,
        Accounts: AccountStack,
        Auth: AuthStack,
    },
    {
        initialRouteName: 'LoadingScreen'
    }
);

export default AccountFlow