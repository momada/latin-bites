import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { FormControl,FormGroup, Validators } from '@angular/forms'
import { RestaurantListComponent } from '../restaurant-list/restaurant-list.component';

@Injectable({
  providedIn: 'root'
})

export class RestaurantService {

    constructor(private firebase: AngularFireDatabase) { }
    restaurantList: AngularFireList<any>;

    form = new FormGroup({
      $key: new FormControl(null),  
      sName: new FormControl('', Validators.required),
      sCountry: new FormControl('')
    });
  
    getRestaurants(){
      this.restaurantList = this.firebase.list('PageTest');
      return this.restaurantList.snapshotChanges();
    }

    insertRestaurant(restaurant) {
      this.restaurantList.push({
        sName: restaurant.sName,
        sCountry: restaurant.sCountry
      });
    }
    
    populateForm(restaurant){
      this.form.setValue(restaurant);
    }

    updateRestaurant(restaurant){
      this.restaurantList.update(restaurant.$key,
        {
          sName: restaurant.sName,
          sCountry: restaurant.sCountry
        });
    }
    
    deleteRestaurant($key : string){
      this.restaurantList.remove($key);
    }
  }

