import { Component, OnInit } from '@angular/core';
import{RestaurantService} from '../shared/restaurant.service'
@Component({
  selector: 'app-restaurant-list',
  templateUrl: './restaurant-list.component.html',
  styleUrls: ['./restaurant-list.component.css']
})
export class RestaurantListComponent implements OnInit {

  constructor(private restaurantService : RestaurantService) { }
  restaurantArray = [];
  showDeletedMessage: boolean;
  searchText: string = "";

  ngOnInit() {
    this.restaurantService.getRestaurants().subscribe(
      list => {
        this.restaurantArray = list.map(item =>{
          return{
            $key: item.key,
             ...item.payload.val()
          };
      });
    });
  }
  onDelete($key){
    if (confirm('Are you sure to delete this record?')){
      this.restaurantService.deleteRestaurant($key);
      this.showDeletedMessage = true;
      setTimeout(() => this.showDeletedMessage = false, 3000);
    } 
  }
  filterCondition(restaurant){
    return restaurant.sName.toLowerCase().indexOf(this.searchText.toLowerCase()) != -1;
    }
}
