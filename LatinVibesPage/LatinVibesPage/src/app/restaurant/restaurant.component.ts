import { Component, OnInit } from '@angular/core';
import {RestaurantService} from '../shared/restaurant.service';

@Component({
  selector: 'app-restaurant',
  templateUrl: './restaurant.component.html',
  styleUrls: ['./restaurant.component.css']
})
export class RestaurantComponent implements OnInit {

  constructor(private restaurantService : RestaurantService) { }
  submitted: boolean;
  showSuccessMessage: boolean;  
  formControls = this.restaurantService.form.controls;

  ngOnInit() {
  }
  
  onSubmit(){
    this.submitted = true;
    if(this.restaurantService.form.valid){
      if(this.restaurantService.form.get('$key').value == null)
        this.restaurantService.insertRestaurant(this.restaurantService.form.value);
      else
        this.restaurantService.updateRestaurant(this.restaurantService.form.value);
      this.showSuccessMessage = true;
      setTimeout(() => this.showSuccessMessage = false, 3000);
      this.submitted = false;
    }
  }
}
